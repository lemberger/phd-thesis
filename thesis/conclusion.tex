\section{Future Research}

%\paragraph{Test Benchmarks}
%Currently, \testcomp and \testcov
%only consider test-generation~tasks
%that use primitive types as test~input (integers, characters, etc.).
%This should be extended in two directions:
%the creation of complex data structures (lists, graphs, etc.),
%and structured data (XML, C~code, etc.)~\cite{Fairfuzz}.
%
%Complex data structures already occur in the sv-benchmarks,
%but they are created from individual, primitive test~inputs.
%To ask test~generators to directly create complex data~types
%
%%The exchange~format for test~suites must be extended for complex data-types,
%%and a mechanism for parsing 
%For structured data, new test-generation~tasks must be created.
%These could be taken from existing open-source parsers or compilers.
%Because it is almost impossible to remove all undefined behavior
%in large real-world C projects,
%and not all bugs in real-world projects are known,
%the generated tasks would most likely be limited to branch coverage,
%and not to finding errors.

\paragraph{Mutation Testing}
So far, we only considered traditional coverage measures as test-adequacy~criteria.
We do consider the bug-finding capabilities of test~generators
when we ask them to generate a test~suite that covers a specific test
(coverage~criterion \emph{coverage-error-call}).
But mutation~testing~\cite{MutationTesting,MutationTestingEvaluation,MutationTestingSurvey2019} goes beyond that:
it not only evaluates whether a test~suite covers a current bug in the program,
but also whether future bugs would be detected.
To include mutation~testing in test-generation~evaluation,
\testcov could be extended to consider the mutation~testing criterion
for either \emph{weakly killed}~\cite{MutationTestingWeak}
or \emph{strongly killed}~\cite{MutationTesting} mutants.
Existing mutation-testing tools~\cite{SRCIROR,MULL,MART} can be used for mutant creation.

\paragraph{Exchange Formats for Formal Verification}
In this work we considered three exchange~formats for formal verification:
condition automata, violation witnesses, and correctness witnesses.
All three are based on finite-state~automata and describe a subspace
of the program-state space.
Unifying the three into a single exchange format may be useful;
this would require verifiers to only implement a single format instead of three,
and it would lessen the restrictions on input/output formats,
which increases the interoperability between verifiers.

In addition, condition~automata should be examined in more detail:
\cpachecker writes a condition~automaton as
a precise projection of the unfinished parts of the explored abstract program-state~space,
based on the currently computed state~space.
These computed state~spaces can be very large.
%and exploration is usually not done in a depth-first search (to not get stuck in a single, very long path).
In consequence, condition~automata can grow very big~(millions of lines of text description).
Existing work~\cite{FRED} approaches the issue
by reducing the size of a created residual program,
but similar techniques may be used one step earlier,
when writing the original condition automaton.
The effect of different state-space traversal strategies should also be considered, to keep
the unfinished parts of the explored abstract program-state~space small; the spectrum
goes from depth-first search (which keeps the description of the unfinished parts as small as possible
because only a single abstract program~path is explored at a time),
to breadth-first search (which creates the maximum number of unfinished parts
as all possible abstract program~paths are considered concurrently).


\paragraph{Residual Programs}
Currently, we create residual programs for formal verification as a product of program and condition automaton. 
This is precise, but may create large residual~programs
because every transition~path in the condition~automaton is directly encoded in the program.
To create smaller residual~programs, other techniques for program reduction could be considered,
for example based on static~program~slicing~\cite{Weiser:1984},
as inspired by other work on conditional model checking~\cite{CzechConditionalModelChecking}.

For test~generation, we create residual~programs through syntactic pruning of fully covered
program sub-trees. More elaborate testability~transformations~\cite{TestabilityTransformation}
could be considered to create smaller residual programs.


\paragraph{Combinations}
To keep the design of our experimental evaluations manageable,
we only considered sequential combinations of verifiers.
%\footnote{The only exception is the cyclic combination
%of a formal~verifier with itself, to turn it into a test~generator (cf. \cref{apx:ATVA19}).}
This means that many potential combinations are yet unexplored.
Notable mentions are cyclic combinations of sequential tools,
which have proven successful~\cite{CoVeriTest-STTT} for test~generation in cohesive implementations,
as well as the decomposition of a verification- or test-generation-task
into a set of smaller tasks.
This has been proven successful for formal verification~\cite{BiAbduction,HiFrog}
and test~generation~\cite{ProgramPartitioning}.
Current techniques for formal~verification provide no exchange format
for decomposition information,
and do not allow the use of different verifiers per sub-task.
Techniques for test~generation do decompose the input~program, 
but have no means to adjust the coverage~criterion accordingly.

Decomposed sub-tasks could also be combined with strategy selection~\cite{StrategySelection,AlgorithmSelection-RepresentationLearning,GUIDO,GRAVES}
to automatically choose a fitting verifier and configuration for each sub-task.

\paragraph{Information Use}

We have only considered cooperation between verification techniques
that work towards the same goal; solving a verification~task,
or solving a test-generation~task.
We discarded all sub-parts already solved
(for example sub-trees of the program-state space that were fully explored
or test goals).
\symbiotic~\cite{SYMBIOTIC-SVCOMP22} also combines
different verification tools, but uses the information differently:
Results of initial (usually fast) analyses are used to
simplify the verification task for a final, precise analysis.
Analyses can also encode information in condition automata
as state-space guards. This information could be used to achieve the same.

In addition, we do not check the soundness of a condition,
but always assume it is correct.
This means that we may introduce irrecoverable imprecisions  
if an imprecise analysis is used.
A mechanism to check imprecisions and refine
them may improve effectiveness~\cite{HiFrog}.


\paragraph{Decomposition of existing Verification Techniques}

We demonstrated the decomposition of existing verification~techniques
on the example of \cegar. More techniques could be decomposed
into individual software units to examine the feasibility of decomposition
on more examples and to achieve the positive effects of decomposition.
Notable mentions are \coveritest~\cite{CoVeriTest-STTT},
bounded model checking~\cite{BMC},
modular verification~\cite{HiFrog,BiAbduction},
and \kinduction~\cite{kInduction,CoVEGI}.



%\clearpage
\section{Conclusion}

The presented research lays the
groundwork for large-scale experimental comparisons in test~generation for C~programs
and demonstrates the potential of cooperation in both formal~verification, test~generation,
and in-between.

(1)~We were the first to do large-scale experimental comparisons of formal verifiers
and test~generators.
Succinctly, we created techniques for \testcomp
that support reliable experimental comparisons of test~generators
for C~programs.
Last, we closed the gap between formal~verifiers and test~generators
by presenting a technique that
turns any formal~verifier of \svcomp into a directed test~generator.

(2)~We explored the potential of
cooperative software~verification
in different directions.
First, we made cooperation with conditions widely usable
by encoding the proprietary condition-automaton~format as program~code.
Then, we introduced conditional~testing
and enabled to turn any formal~verifier into a test~generator for coverage~criteria.
On the example of incremental~verification, we
showed the flexibility of conditions.
Last, we decomposed \cegar into stand-alone components
to demonstrate how existing techniques for software~verification can be decoupled.

Our work provides a fundamental contribution towards cooperative software verification
with automated test generation and automated formal verification,
and, to this end, a fundamental contribution to automated software verification in general.

\paragraph{Availability}
Whenever feasible, our research is published with open access.
We provide reproduction packages and the raw data of our experiments for each
article.

We provide multiple links to web pages in this thesis.
Because things on the internet rarely last forever,
there are two ways to access archived versions of the web pages:
(1)~If the URL is to a software repository on \texttt{github.com}
or \texttt{gitlab.com},
use the \href{https://archive.softwareheritage.org/}{Software Heritage Archive}:
enter the URL of the repository in the search field at
{\small\url{https://archive.softwareheritage.org/}}.
For example, for the original software~repository {\small\url{https://gitlab.com/sosy-lab/software/cpachecker/}},
you can access its archive through
{
    \let\oldUrlBreaks\UrlBreaks
    \renewcommand{\UrlBreaks}{\oldUrlBreaks\do\u\do\r\do\l}
{\small\url{https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.com/sosy-lab/software/cpachecker.git}}.
    \let\UrlBreaks\oldUrlBreaks
}
(2)~For all other URLs, use the \href{https://web.archive.org/}{Internet Archive Wayback Machine}:
prefix the original URL with {\small\url{https://web.archive.org/web/}}
to get the latest archived web-page version.
For example, for the original URL {\small\url{https://test-comp.sosy-lab.org/2022/}},
you can access its archive through {\small\url{https://web.archive.org/web/https://test-comp.sosy-lab.org/2022/}}.

The source of this thesis is available at {\small\url{https://gitlab.com/lemberger/phd-thesis}}.
