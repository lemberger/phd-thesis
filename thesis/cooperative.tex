\begin{figure}[t]
    \begin{minipage}[t]{.32\linewidth}
        \begin{minted}[frame=none,fontsize=\footnotesize]{c}
int out;
int val = nondet_int();
if (val >= 0) {
  out = val%2 * val%3
} else {
  out = -val;
}
if (out < 0) {
    reach_error();
}
        \end{minted}
    \end{minipage}\hspace{-7mm}%
    \begin{minipage}[t]{.3\linewidth}
        % required to properly set baseline above tikzpicture
        \strut\vspace*{-\baselineskip}\newline
        \begin{tikzpicture}[condLabel/.append style={font=\small}]
            \node[condNode, initial] (0) {$q_0$};
            \node[condNode, below = 4mm of 0] (1) {$q_1$};
            \node[condNode, below right = 7mm and 2mm of 1, accepting] (f) {$q_{f}$};
            \node[condNode, below left = 7mm and 2mm of 1] (2) {$q_2$};
            %\node[condNode, below = 6mm of 2] (3) {$q_3$};
            %\node[condNode, below = 6mm of 3] (4) {$q_4$};

            \path
                (0) edge[condEdge]
                    node[right, condLabel] {$\{(l_2, \cdot, l_3)\}, true$}
                    (1)
                (1) edge[condEdge]
                    node[right, condLabel] {$\{(l_3, \cdot, l_6)\}, true$}
                    (f)
                (1) edge[condEdge]
                    node[left, condLabel] {$\{(l_3, \cdot, l_4)\}, true$}
                    (2)
                (f) edge[condEdge, loop below]
                    node[below, condLabel] {$E, true$}
                    (f)
                (2) edge[condEdge, loop below]
                    node[below, condLabel] {$E, true$}
                    (f)
            ;
        \end{tikzpicture}\\
   \end{minipage}\hfill%
    \begin{minipage}[t]{.32\linewidth}
        \begin{minted}[frame=none,fontsize=\footnotesize]{c}
int out;
int val = nondet_int();
if (val >= 0) {
  out = val%2 * val%3
  if (out < 0) {
    reach_error();
  }
} else { }
        \end{minted}
    \end{minipage}\null\vspace{2mm}%

    \parbox[b]{.3\linewidth}{
        \subcaption[]{Program\label{fig:cmc-program}}
    }\hfill%
    \parbox[b]{.3\linewidth}{
        \subcaption[]{Condition\label{fig:cmc-condition}}
    }\hfill%
    \parbox[b]{.3\linewidth}{
        \subcaption[]{Residual program\label{fig:cmc-residual}}
    }

    \caption{Reduction of original program and condition into residual program}
    \label{fig:cmc-example}
\end{figure}


\section{Encoding Condition Automata}
The idea of conditional~verification~\cite{ConditionalModelChecking}
not only proposed condition automata, but also an exchange~format for these.
To the best of our knowledge,
only \cpachecker 
supports this format to this date.
But for successful cooperation, we need more than a single verifier.
Instead of looking into reasons why the format for condition~automata
is not adapted by other verifiers, and instead of proposing a new format
for conditional~verification,
we decided to use an existing format that every
verifier understands: program source code.

\paragraph{Condition to Code}
In the article~``\citetitle{ICSE18}''~(\cref{apx:ICSE18}),
we define the notion of a \emph{reducer}:
A reducer takes a program~$\symCfa$ and a condition~$\symcondition$
for~$\symCfa$
and creates a new residual program~$\symCfa_{r}$
that only contains
those program executions that are not already covered by~$\symcondition$.
\Cref{fig:cmc-example} shows an example for this:
The program in \cref{fig:cmc-program}
and the condition in \cref{fig:cmc-condition}
can be combined into a new residual~program~(\cref{fig:cmc-residual}).
This residual program does not contain
any of the original program~executions
that go through the else-branch
(line~6 in \cref{fig:cmc-program}).

\paragraph{Combining Formal Verifiers}
By encoding the information of the condition in a new program,
we can use any off-the-shelf verifier instead of a specialized
conditional~verifier.
We perform a large experimental evaluation with three formal verifiers:
\cpachecker, \smack and \uautomizer.
Each of the three verifiers~$A$
is compared with a combination of \cpachecker's predicate~abstraction
and the reducer-based construction applied to~$A$.
The results are encouraging: we not only prove the feasibility of
the reducer-based construction of conditional~verifiers,
but we also show the potential benefits of
now-possible tool~combinations:
Each of the tool combinations can solve significantly more tasks
than the respective stand-alone tool,
and the overall effectiveness increases: Thanks to the tool~combinations,
we can now solve verification~tasks that none of the stand-alone tools
could solve before.
%
\paragraph{Combining Formal Verifier and Test Generator}
Last, we show an additional benefit of encoding the condition in the program:
the concept of conditional~verification, previously only used
as conditional~model~checking with formal verifiers, is now applicable
to arbitrary tools that analyze software. As example, we use test~generators:
We consider three test~generators, \afl, \crestppc, and \klee.
We combine \cpachecker's predicate abstraction with these through our reducer-based construction,
and show that the test~generators' effectiveness in finding errors on the residual programs
increases compared to the original programs.

\paragraph{Later Developments}
The introduction of residual programs enables an array of new applications:
Subsequent work~\cite{FRED} explores different detail levels of residual programs.
Difference~verification~(\cref{apx:SEFM20B}), an application of condition automata,
is usable with any off-the-shelf verifier thanks to residual programs.
\metaval~\cite{MetaVal} transfers the idea of encoding information as source~code
to violation-witness automata and correctness-witness automata,
and turns any given off-the-shelf verifier into a witness validator.
And component-based \cegar~(\cref{apx:ICSE22}) uses \metaval to
turn any off-the-shelf verifier into one of its components.

\section{Cooperation between Test-Generators}
\newcommand{\symgoals}{\psi}

Through the reducer-based construction of conditional~verifiers,
we can use test~generators as a second component in conditional~verification
to solve a verification~task.
But we can not yet combine two test~generators to solve a test-generation task.

\paragraph{Conditional Testing}
In the article~``\citetitle{ATVA19}''~(\cref{apx:ATVA19}),
we transfer the idea of both conditional~verification and residual programs
to automated test~generation:
If a test~generator is not able to create a test~suite that fulfills
a coverage~measure to 100\,\%,
we compute the remaining coverage~goals and represent them as a condition.
A conditional~test~generator can then take this condition and only generate
tests for the remaining coverage goals.
As condition, we use a type tailored to test~generation:
We express conditions as the set of remaining coverage~goals.
Because the notion of a \emph{conditional test generator}
is newly proposed, there are no supporting tools yet.
\begin{figure}[t]
    \parbox{.32\linewidth}{%
    \raggedright
    \scalebox{.8}{%
       \begin{tikzpicture}[
         node distance=6mm,
         verdict/.append style={minimum width=3cm},
         ]
       \node[artifact program] (prog) {$\symCfa$};
       \node[artifact condition, below = 6mm of prog] (condition) {$\symcondition$};

       %\draw[dashed] (rect-start) rectangle ($(property.south east) + (2mm, -2mm)$);
       %\node[below = 2mm of criterion, align=center, text width=2cm] {\footnotesize Verification Task};

       \coordinate (middle) at ($(prog.east)!0.5!(condition.east)$);
       \node[actor, right = of middle] (reducer) {Reducer};
       \node[artifact program, right = of reducer] (residual) {$\symCfa_{r}$};

       \path
           (prog) edge[flow] (reducer)
           (condition) edge[flow] (reducer)
           (reducer) edge[flow] (residual)
           ;
       \end{tikzpicture}%
    }
    }\hfill%
    \parbox{.32\linewidth}{%
    \centering
    \scalebox{.8}{%
       \begin{tikzpicture}[
         node distance=6mm,
         verdict/.append style={minimum width=3cm},
         ]
       \node[artifact program] (prog) {$\symCfa$};
       \node[artifact criterion, below = 6mm of prog] (criterion) {$\symtestcriterion$};
        \node[below = 6mm of criterion, inner sep=0pt] (suite) {%
            \begin{tikzpicture}
                \node[artifact tests] (test-3) {};
                \node[artifact tests, xshift=-2pt, yshift=-2pt] (test-2) {};
                \node[artifact tests, xshift=-4pt, yshift=-4pt] (test-1) {$\symtestsuite$};
            \end{tikzpicture}%
        };

       %\draw[dashed] (rect-start) rectangle ($(property.south east) + (2mm, -2mm)$);
       %\node[below = 2mm of criterion, align=center, text width=2cm] {\footnotesize Verification Task};

       \coordinate (middle) at (criterion.east);
       \node[actor, right = of middle] (extractor) {Test-Goal\\ Extractor};
       \node[artifact goals, right = of extractor] (goals) {$\symgoals$};

       \path
           (prog) edge[flow] (extractor)
           (criterion) edge[flow] (extractor)
           (suite) edge[flow] (extractor)
           (extractor) edge[flow] (goals)
           ;
       \end{tikzpicture}%
    }
    }\hfill%
    \parbox{.32\linewidth}{%
    \raggedleft
    \scalebox{.8}{%
       \begin{tikzpicture}[
         node distance=6mm,
         verdict/.append style={minimum width=3cm},
         ]
       \node[artifact program] (prog1) {$\symCfa_1$};
       \node[artifact program, below = 6mm of prog] (prog2) {$\symCfa_2$};

       %\draw[dashed] (rect-start) rectangle ($(property.south east) + (2mm, -2mm)$);
       %\node[below = 2mm of criterion, align=center, text width=2cm] {\footnotesize Verification Task};

       \coordinate (middle) at ($(prog1.east)!0.5!(prog2.east)$);
       \node[actor, right = of middle] (diffcond) {Difference\\Computation};
       \node[artifact condition, right = of diffcond] (cond) {$\symcondition$};

       \path
           (prog1) edge[flow] (diffcond)
           (prog2) edge[flow] (diffcond)
           (diffcond) edge[flow] (cond)
           ;
       \end{tikzpicture}%
    }
    }\hfill%
    \vspace{3mm}
    \parbox[t]{.3\linewidth}{%
    \caption{Reducer}
    \label{fig:reducer}
    }\hfill%
    \parbox[t]{.3\linewidth}{%
    \caption{Test-Goal Extractor}
    \label{fig:extractor}
    }\hfill%
    \parbox[t]{.3\linewidth}{%
    \caption{Difference Computation}
    \label{fig:diffcond}
    }
\end{figure}
To remedy this, we define program~reduction and condition~generation for conditional~testing:
\paragraph{Program Reduction}
We define the notion of program~reducer in the context of conditional testing.
A reducer~(\cref{fig:reducer}) for coverage~goals is a testability transformation~\cite{TestabilityTransformation}
that takes a program~$\symCfa$ and a condition~$\symcondition$ (in the form of a set of coverage~goals),
and returns a residual program~$\symCfa_{r}$.
We require a program~reducer to be \emph{sound} and \emph{complete}
to be able to fulfill the original test-generation task across combinations.
Both attributes are defined in \cref{apx:ATVA19}.

We show three~exemplary types of reducers: the identity (the program stays unchanged),
a program~reducer that uses syntactic pruning of exhaustively covered program parts,
and a program~reducer that annotates relevant test~goals in the program,
for example to prove their (in-)feasibility with formal verifiers.

\paragraph{Condition Generation}
We define the notion of a test-goal extractor:
A test-goal extractor~(\cref{fig:extractor}) takes a program~$\symCfa$,
a coverage~criterion~$\symtestcriterion$ and a test~suite~$\symtestsuite$,
and returns the set~$\symgoals$ of covered test~goals of $\symtestcriterion$.
%
The information of a \emph{sound} and \emph{complete} test~goal extractor
(again, both attributes are defined in \cref{apx:ATVA19})
can then be used to generate a condition
by computing the set of all test~goals described by $\symtestcriterion$,
and removing all already covered test goals from that set.
At first sight it may seem like the construction of a condition would be easier
if the test-goal extractor provided the number of currently uncovered test~goals
directly.
But,
(a)~this would not fit the existing definition of conditions,
which are required to encode the covered
verification space,
and
(b)~this would require the test-goal extractor to always know about all covered test goals,
either through re-execution of all generated tests or by being stateful.
%We decided to keep the test-goal extractor conceptually simple and flexible,
%and in turn perform the condition~computation outside of this component.

We instantiate a test-goal extractor
based on program instrumentation, test~execution and line-coverage measurement.

\paragraph{Combinations}
To get access to a large selection of tools, we use the formats
that are defined by \testcomp
and supported by all \testcomp participants.
With program~reducer and test-goal extractor,
this gives access to a large selection of off-the-shelf
test~generators as conditional testers.

The introduction of conditional testing allows
different flavors of combinations between test~generators;
we propose a few conceptually (e.g., sequential, cyclic, parallel),
and then go on to show the potential benefit of conditional testing
through experiments
on sequential~combinations of pairs of test generators.


\paragraph{Turning Formal Verifiers into Test Generators}
In addition, we show how to turn any formal~verifier into a test~generator
through the witness-to-test generation~\cite{TAP18}
and a cyclic conditional-testing composition
that restarts the formal verifier and the witness-to-test generation
until all test goals are covered or proven infeasible.
%This idea was also used by abstraction-driven testing~\cite{AbstractionConcolicTesting},
%\coveritest~\cite{CoVeriTest-STTT}
%and \fusebmc~\cite{FusebmcTap}.

\paragraph{Later Developments}
We implemented conditional~testing in the tool~\condtest.
Initially, \condtest was  a stand-alone tool for composition of
different test generators and formal verifiers for test~generation.
Since \href{https://gitlab.com/sosy-lab/software/conditional-testing/-/tree/v3.0}{version~3.0},
the composition moved to 
\href{https://gitlab.com/sosy-lab/software/coveriteam/-/tree/5307183587525e57ed143d814dbbc46f0eeceba7/examples/CondTest}{%
        \coveriteam}~\cite{COVERITEAM},
and \condtest provides the reducers and test-goal extractor.


\section{Condition Automata for Difference Verification}

Condition automata specify the program-state~space 
a verifier must prove safe.
Originally, conditional~model checking was proposed to make
formal verifiers cooperate, but the control-mechanism
of condition automata can also be used for other purposes:
In the article~``\citetitle{SEFM20B}''~(\cref{apx:SEFM20B}),
we show the versatility of condition automata
by using them for difference verification.

Assume that we have two revisions of a program, rev.~1 and rev.~2.
We trust rev.~1,
for example because it was fully verified or has been running
in production for a long time without issues.
Now, we want to verify rev.~2.
Since we already trust rev.~1, we do not need to fully re-verify
rev.~2---instead, it is enough to only verify those parts
that might have changed from rev.~1.

We introduce an algorithm, \diffcond{}, that
takes two revisions of a software code
and creates a condition~automaton
that encodes the program-state~space that
is unaffected by the changes
from one revision to the other~(\cref{fig:diffcond}).
This condition can then be given to a conditional verifier
to only verify the parts of the program that may be affected by the change.

We show the benefits of this approach on incremental verification
on a large number of benchmark tasks that we have generated from existing,
strongly~coupled programs. These new benchmark~tasks were adopted
by the \svcomp iterations following this article,
in the new category \emph{ReachSafety-Combinations}.

\section{Decomposing Verification Techniques}

\begin{figure}[t]
  \centering
  \scalebox{.8}{%
  \begin{tikzpicture}[actor/.append style={text width=2.5cm},
    circState/.style={draw, circle, inner sep=0pt, minimum width=4.5mm}]
    
    \node[actor, anchor= south] (abstraction) at (1.5,0) {Abstract-Model\\ Explorer};
    \node[actor, anchor=south west] (cex-check) at (3, -3.0) {Feasibility\\ Checker};
    \node[actor, anchor=south east] (refinement) at (0, -3.0) {Precision\\ Refiner};

    \node[artifact progCrit, left = 2cm of abstraction.west] (prog)    {$\symCfa, \symproperty$};
    \node[below = 1mm of prog.south, anchor=north, align=center, scale=.8] (prog-label) {Verification\\ Task};

    % \node[draw, circle, minimum size=4.8cm] at (1.5,-1.5) {};
    \node[fill=colorTrue, minimum width=1.8cm, minimum height=1.8cm,
      text depth=1cm, font=\small,
      right = 2cm of abstraction.east] (correct) {$\symCfa \models \symproperty$};
    \node[fill=colorFalse, minimum width=1.8cm, minimum height=1.8cm,
      text depth=1cm, font=\small, below = 3.5cm of correct
    ] (incorrect) {$\symCfa \not\models \symproperty$};

    %\node[below = 1mm of red.south, anchor=north] (verdict-label) {Verdict};

    \node[artifact, scale=.7, above = 2mm of correct.south] (proofwitness) {$\symcorrectnesswitness$};
    \node[artifact, scale=.7, above = 2mm of incorrect.south] (violationwitness) {$\symviolationwitness$};

    \begin{pgfonlayer}{background}

    \path ($(abstraction.south east) + (0,  .53)$) edge[flow, bend left=28]
          node[artifact violation witness, scale=.8, pos=0.3] (maybe-vw) {$\symviolationwitness$}
          node[circState, fill=colorUnknown, pos=0.8] (qm) {?}
      ($(cex-check.north west) + (.89,0)$);

    \path ($(cex-check.south) + (-.88, 0)$) edge[flow, bend left=54]
            node[circState, fill=colorFalse, pos=0.1] (xm) {\xmark}
          node[artifact path witness, scale=.8, pos=0.5] (no-vw) {$\symviolationwitness$}
      ($(refinement.south east) + (-.36,0)$);

    \path ($(refinement.north east) + (-.9, 0)$) edge[flow, bend left=26]
          node[artifact correctness witness, scale=.8, pos=0.4] {$\symcorrectnesswitness$}
      ($(abstraction.south west) + (0,.55)$);
    
    \path (abstraction.west |- correct) edge[flow] (correct);
    \path (prog.west |- correct) edge[flow] (abstraction.west|- correct);
    \path[draw, thick] (cex-check.south)  --
        node[circState, fill=colorConfirmed, pos=0.2] (cm) {\cmark}
        (incorrect.west -| cex-check)
        (incorrect.west -| cex-check) edge[flow] (incorrect.west)
        ;
    \end{pgfonlayer}

  \end{tikzpicture}
  }
  \caption{Component-based \cegar}
  \label{fig:cegar}
\end{figure}

The idea of multiple components working together towards
a single verification~task is visible in multiple traditional verification~techniques,
like \cegar~\cite{ClarkeCEGAR}, \kinduction~\cite{K-Induction},
and static program slicing~\cite{Weiser:1984,SystemDependenceGraph}.
To increase flexibility, ease maintenance
and make it easier to see conceptual similarities~\cite{AlgorithmComparison-JAR},
we propose to decompose existing techniques.
We do this on the example of \cegar~\cite{ClarkeCEGAR}.

\paragraph{CEGAR}
\cegar consists of three steps:
(1)~An abstract model of the program-under-verification is explored;
the aim is to prove it correct with regards to a property $\symproperty$,
or find a counterexample to $\symproperty$.
Usually, at the beginning of a verification run, this abstract model is very coarse.
If the abstract model is correct, the verification task is solved~$\symCfa \models \symproperty$.
(2)~If a counterexample is found,
it is checked for feasibility with a high-precision technique.
If the counterexample is confirmed feasible,
the verification task is solved $\symCfa \not\models \symproperty$.
(3)~If the counterexample is infeasible,
the abstract model is made more precise so that this spurious counterexample
is not encountered again in subsequent abstract-model explorations.
After this precision refinement, the cycle repeats at~(1) with the new,
more precise model.
These three steps are traditionally implemented as a single algorithm.


\paragraph{Decomposition of CEGAR}
In the article~``\citetitle{ICSE22}''~(\cref{apx:ICSE22}),
we show that the three steps of \cegar can be defined as
individual software~components
that exchange information through verification~artifacts.
We call this concept \emph{component-based \cegar}~(\cref{fig:cegar}).

%\paragraph{Off-the-shelf Tools in CEGAR}
For information exchange, we 
show how to use the
%existing and widely~supported
exchange formats for verification~witnesses.
This enables us to use existing tools
in any of the three steps of component-based \cegar:
Correctness-witness~validators as abstract-model explorer,
violation-witness validators as feasibility~checker,
and
violation-witness validators that produce correctness~witnesses as precision~refiner.

We implement a selection of different \cegar~compositions
in \coveriteam~\cite{COVERITEAM},
and show through an experimental evaluation that
the technical disadvantages of this decomposition are manageable:
there is only a constant factor of run-time decrease
compared to the natively implemented \cegar in \cpachecker.

Component-based \cegar enables developers to exchange \cegar~components
in a plug-and-play fashion.
We hope that this not only avoids the lock-in effect, but also accelerates
future research in \cegar
and serves as a positive example of decomposing existing verification techniques.
