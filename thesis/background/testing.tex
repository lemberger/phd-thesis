\newcommand{\symtestcriterion}{\varphi}

\paragraph{Software Tests}
A test~input~$t = \langle v_0, \ldots, v_n\rangle$
is a sequence of $n$~input~values for a single program execution.
When an input variable
gets assigned a new non-deterministic value
during program execution with $t$,
the next input~value $v_i$ is used for the assignment.
For example, given
the program

\begin{minted}[frame=none]{C}
int a;
for (int i = 0; i < 2; i++) {
    a = nondet();
}
\end{minted}

\noindent
and test~input~$t = \langle 224, 65 \rangle$,
the program execution with~$t$
first assigns 224 to~\mintinline{C}{a},
and then assigns 65 to \mintinline{C}{a}.
Given the program

\begin{minted}[frame=none]{C}
int a = nondet();
int b = nondet();
\end{minted}

\noindent
and the same $t$, the
program execution with $t$ assigns 224 to~\mintinline{C}{a}
and 65 to~\mintinline{C}{b}.


A software~test
consists of a test~input and an expected program~behavior.
%The program is executed with the given input values to observe its actual behavior.
%If the actual behavior equals the expected behavior, the test passes.
%Otherwise, the test fails.
In our work, the expected program~behavior is extrensic to the test:
When we \emph{generate a test}, only the test~input is generated,
no expected behavior for this input.
The expected behavior
is either available separately
(e.g., function \mintinline{c}{reach_error} may never be called,
there may be no unsigned overflow in the program),
or implied (e.g., the program should never crash).
%For the sake of simplicity,
From now on we use the term \emph{test}
interchangeably with the term \emph{test input}.
A collection of tests is called a \emph{test suite}.

\paragraph{Coverage Goals}
%Testing all possible program executions is infeasible for most programs,
It is impossible to proof program~safety through testing
for all but the simplest programs.
Instead, the confidence in a test suite's expressiveness
is indicated with \emph{adequacy~criteria}.
Adequacy criteria can be based on different information:
Examples are a behavior~specification of the program
(e.g., category partitions or state transitions),
extrinsic information
(e.g., seeded~faults or program~mutations~\cite{MutationTesting,MutationTestingAtFacebook,MutationTestingSurvey2019}),
or the program structure (e.g., error methods, branch coverage,
or modified condition/decision coverage~\cite{McdcAnalysis}).
We focus on the latter.
%
For adequacy~criteria based on the program structure,
the required and actual code coverage can be precisely measured%
---because of this, we call these \emph{coverage~criteria}.
The specification language~\fql{}~\cite{FQL}
provides flexible means to formally define them.
A coverage~criterion consists of a set of \emph{coverage goals}.
A coverage goal is a projection on the CFA:
a program location, edge, or any combination thereof~\cite{FQL}.
To cover a coverage goal, a test must cover all components of the coverage goal
in a single execution. For example,
coverage goal \fqlcode{@}$\symloc_3$ (written in \fql{})
requires a test's~execution
to pass through~$\symloc_3$,
and coverage goal \fqlcode{@}$\symloc_3$\fqlcode{.@}$\symloc_4$\fqlcode{.@}$\symloc_4$
requires a test's~execution to first pass through $\symloc_3$
and then at least two times through~$\symloc_4$.
For the sake of presentation,
we only consider single program locations (like \fqlcode{@}$\symloc_3$)
as coverage goals.
Program transformations~\cite{TestabilityTransformation}
can be used to map the coverage~goals of a coverage~criterion
from one to another.

\newsubfloat{figure}
\begin{figure}[t]
  \begin{minipage}[b]{.49\linewidth}
    \begin{tikzpicture}[covered/.style={}, testinput/.style={opacity=0}]
      \input{background/figures/cfa.tex}
    \end{tikzpicture}
    \subcaption[]{Program with two sets of coverage goals:
        Coverage goals for branch coverage are $\symloc_4$, $\symloc_7$, $\symloc_9$, $\symloc_{10}$.
        Coverage~goal for \mintinline{c}{reach_error()} is $\symloc_9$.
        \label{fig:coverageGoals}}
  \end{minipage}\hfill%
  \begin{minipage}[b]{.49\linewidth}
    \begin{tikzpicture}[]
      \input{background/figures/cfa.tex}
    \end{tikzpicture}
    \subcaption[]{Test coverage recorded during execution
    of test with input value `0':
    $\symloc_1, \symloc_2, \symloc_3, \symloc_7, \symloc_{10}$%
    \label{fig:coverageMeasurement}
    }
  \end{minipage}
  \caption{Example for coverage measurement}
  \label{fig:coverage}
\end{figure}

\newcommand{\symtestsuite}{\mathcal{TS}}

We first consider the coverage~criterion
that all calls to function~\mintinline{c}{reach_error} are covered,
defined in \fql{} as \fqlcode{COVER EDGES(@CALL(reach_error))}.
For the program from \cref{fig:cfa},
the only call to~\mintinline{c}{reach_error} is at
$\symloc_9$.
Let us assume we had a test suite~$\symtestsuite_{\{0\}}$ that contains a single test
with input value~0 for the single input variable~\texttt{n}.
\Cref{fig:coverageMeasurement} shows in green all program locations
that $\symtestsuite_{\{0\}}$ covers:
$\symloc_1$, $\symloc_2$, $\symloc_3$, $\symloc_7$, and $\symloc_{10}$.
Since it does not cover $\symloc_9$,
$\symtestsuite_{\{0\}}$ is not an adequate test~suite.
(Note that testing can not prove that $\symloc_9$ is actually never reachable.)

Next, we consider the coverage~criterion \emph{branch coverage}, also known as decision coverage:
It requires that all branches in the program
are covered by the test suite,
defined in \fql{} as \fqlcode{COVER EDGES(@DECISIONEDGE)}.
In \cref{fig:coverageGoals},
the coverage goals for branch coverage
are highlighted with a dashed, orange outline:
\fqlcode{@}$\symloc_4$, \fqlcode{@}$\symloc_7$, \fqlcode{@}$\symloc_9$,
and \fqlcode{@}$\symloc_{10}$.
Test suite~$\symtestsuite_{\{0\}}$ covers 2~out of~4 goals:
\fqlcode{@}$\symloc_7$ and \fqlcode{@}$\symloc_{10}$.
This is reported as 50\,\% coverage.

\paragraph{Test Generation}
\begin{figure}[t]
    %\begin{minipage}{.48\linewidth}
    %    \begin{tikzpicture}[node distance=7mm,
    %        verdict/.style={
    %            draw, ellipse, minimum height=5mm, minimum width=1.5cm
    %        }
    %    ]
    %    \node[artifact] (inputs) {I};
    %    \node[actor, right = of inputs] (program) {Program Execution};
    %    \node[artifact nodraw, right = of program] (output) {Actual Behavior};
    %    \path
    %        (inputs) edge[flow] (program)
    %        (program) edge[flow] (output)
    %        ;
    %        

    %    \node[artifact nodraw, below = of inputs] (actual) {Actual Behavior};
    %    \node[artifact nodraw, right = of actual] (assert) {Expected Behavior};
    %    \node (comparison) at ($(actual)!0.5!(assert)$) {\large $\stackrel{?}{=}$};
    %    \coordinate[right = of assert] (anchor);
    %    \node[verdict, above = 2mm of anchor, fill=colorFalse, anchor=south west] (red) {Fail};
    %    \node[verdict, below = 2mm of anchor, fill=colorTrue, anchor=north west] (green) {Pass};
    %    \coordinate[left = 5mm of anchor] (crossing);
    %    \path[draw, thick]
    %        (assert) -- (crossing) edge[flow] (red)
    %        (crossing) edge[flow] (green)
    %        ;


    %    \draw[dotted] ($(inputs.north west) + (-6mm, 4mm)$) rectangle ($(green.south east) + (6mm, -4mm)$);
    %    \end{tikzpicture}
    %    \caption{Software Test}
    %    \label{fig:test}
    %\end{minipage}\hfill%
    \parbox[b]{0.48\linewidth}{%
        \begin{tikzpicture}[node distance=7mm]
        \node[artifact program] (prog) {$\symCfa$};
        \node[artifact criterion, below = 6mm of prog] (criterion) {$\symtestcriterion$};
        \coordinate (rect-start) at ($(prog.north west) + (-2mm, 2mm)$);

        \draw[dashed] (rect-start) rectangle ($(criterion.south east) + (2mm, -2mm)$);
        \node[below = 3mm of criterion, align=center] {\footnotesize Test-Generation\\\footnotesize Task};

        \coordinate (middle) at ($(prog.east)!0.5!(criterion.east)$);
        \node[actor, right = of middle] (tester) {Test \mbox{Generator}};
        \node[right = of tester, inner sep=0pt] (test-suite) {%
            \begin{tikzpicture}
                \node[artifact tests] (test-3) {};
                \node[artifact tests, xshift=-2pt, yshift=-2pt] (test-2) {};
                \node[artifact tests, xshift=-4pt, yshift=-4pt] (test-1) {$\symtestsuite$};
            \end{tikzpicture}%
        };
        %\node[below = 2pt of test-suite.south, anchor=north] (label) {Test Suite};

        \path
            (prog) edge[flow] (tester)
            (criterion) edge[flow] (tester)
            (tester) edge[flow] (test-suite)
            ;
            
        \end{tikzpicture}
    }\hfill%
    \parbox[b]{0.48\linewidth}{%
        \begin{tikzpicture}[node distance=7mm]
        \node[artifact program] (prog) {$\symCfa$};
        \node[below = 6mm of prog, inner sep=0pt] (test-suite) {%
            \begin{tikzpicture}
                \node[artifact tests] (test-3) {};
                \node[artifact tests, xshift=-2pt, yshift=-2pt] (test-2) {};
                \node[artifact tests, xshift=-4pt, yshift=-4pt] (test-1) {$\symtestsuite$};
            \end{tikzpicture}%
        };
        % required to set figure at same position as fig:test-generator
            \coordinate (rect-start) at ($(prog.north west) + (-2mm, 2mm)$);
            \draw[opacity=0] (rect-start) rectangle ($(test-suite.south east) + (2mm, -2mm)$);
            \node[below = 2mm of test-suite, anchor=north west, align=center, opacity=0, yshift=4pt, text width=4cm] {\footnotesize Test-generation Task};
        % end required
        \coordinate (middle) at ($(prog.east)!0.5!(test-suite.east)$);
        \node[actor, right = of middle] (eval) {Test Oracle\\{\footnotesize (for $\symtestcriterion$)}};

        \node[verdict, right = of eval, fill=colorUnknown] (yellow) {\%};
        \node[verdict, above = 2mm of yellow, fill=colorTrue] (green) {True};
        \node[verdict, below = 2mm of yellow, fill=colorFalse] (red) {False};
        \coordinate[left = 5mm of yellow] (crossing);


        \path
            (prog) edge[flow] (eval)
            (test-suite) edge[flow] (eval)
            ;
        \path[draw, thick]
            (eval) -- (crossing) edge[flow] (red)
            (crossing) edge[flow] (green)
            (crossing) edge[flow] (yellow)
            ;
            
        \end{tikzpicture}
    }
    \vspace{0.5\onelineskip}
    \null\hfill\parbox[t]{0.48\linewidth}{%
        \caption{Test generation for program~$\symCfa$
            and coverage~criterion~$\symtestcriterion$}
        \label{fig:test-generator}
    }\hfill
    \null\hfill\parbox[t]{0.48\linewidth}{%
        \caption{Test-suite adequacy evaluation with a test oracle}
        \label{fig:adequacy}
    }
\end{figure}

A test generator aims to generate a test suite 
for a given program and a given coverage~criterion~(\cref{fig:test-generator}).
While a software developer that writes tests manually
can be considered a test~generator,
we only consider automated generators that are not guided by any user feedback.
Different approaches exist for test generation~%
\cite{AFLfast,CoVeriTest-STTT,CUTE,DART,HYBRIDTIGER-TESTCOMP20,FusebmcTap,HybridConcolicTesting,KLEE,Legion-ASE20,LIBKLUZZER-TESTCOMP20,PRTEST19,SymbioticApproach,TRACER}.
From purely random~\cite{PRTEST19},
over coverage-guided fuzzing~\cite{AFLfast,VERIFUZZ-TESTCOMP22},
to exhaustive state-space exploration~\cite{CoVeriTest-STTT,FusebmcTap,KLEE}.
We do not propose new generation~strategies,
but use, combine, and evaluate existing tools.

\paragraph{Test Oracle}
A \emph{test~oracle}~(\cref{fig:adequacy}) for a given adequacy~criterion receives
the program~under~test and a test~suite and returns whether (or to which degree)
this test~suite fulfills the adequacy~criterion on the program~under~test.
Up to now~(2022), all four~editions of \testcomp
use our tool~\testcov~(\cref{apx:ASE19})
as test~oracle.
\begin{figure}[t]
    \inputminted{xml}{background/figures/metadata.xml}
    \vspace*{-2mm}

    \caption{The \texttt{metadata.xml} of a \testcomp{}~2022 test~suite}
    \label{fig:metadataxml}
    \inputminted{xml}{background/figures/test.xml}
    \vspace*{-2mm}

    \caption[%
    Shortened test XML~of a \testcomp{}~2022 test~suite
    ]{%
    Shortened test XML~of a \testcomp{}~2022 test~suite.
    Typing values is optional.%
    }
    \label{fig:testxml}
\end{figure}

\paragraph{Test-Suite Representation}

The
\href{https://test-comp.sosy-lab.org/2019/}{First International Competition on Software Testing~(\testcomp~2019)}~%
\cite{TESTCOMP19}
introduced a common format
for test~suites.
In this format, a test~suite is represented by multiple files:
%(usually shipped as a single zip archive):
A \texttt{metadata.xml} (\cref{fig:metadataxml})
provides information about the test suite,
and for each test there is one individual file (\cref{fig:testxml})
that defines this test's input values.
All tools that participate in \testcomp must
output generated test~suites in this format.
This improves comparability
and enables an easier integration of
test~generators in combination frameworks~(\cref{apx:ATVA19}).