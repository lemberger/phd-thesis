%\paragraph{Empirical Science}
%Each of our research~publications consists of the following parts:
%an introduction,
%a background, %required to understand the proposed concept,
%the proposed concept,
%an experimental evaluation, % of the proposed concept,
%a conclusion, and
%a discussion of related work (after the introduction or before the conclusion).

Often, we show the effect of
the proposed concepts and tools through experimental evaluations
and comparison with the state of the art.
For this, we follow the methodology of the
International Competition on Software Verification~(\svcomp)~\cite{SVCOMP22}
and the International Competition on Software Testing~(\testcomp)~\cite{TESTCOMP22},
explained below.


\paragraph{Benchmark Set}

Throughout our work,
we use the sv-benchmarks\footnote{\url{https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/}}
benchmark set---the largest available benchmark set for automated software~verification
on C~programs.
In sv-benchmarks, a benchmark~task can be
a \emph{verification task} or a \emph{test-generation task}.
A verification~task~\cite{SVCOMP15} consists of a program (given as C~source code) and
a program~property to check.
The benchmark~task also specifies the expected verification~verdict.
Multiple program~properties exist in sv-benchmarks;
our work focuses on the reachability property \benchProperty{unreach-call},
which specifies that a certain error method
(\mintinline{c}{__VERIFIER_error} or \mintinline{c}{reach_error})
may never be called.

A test-generation~task~\cite{TESTCOMP21} consists of a program (given as C~source code)
and a coverage~criterion that should be fulfilled by a generated test~suite.
Two coverage~criteria exist:
Criterion~\benchProperty{coverage-error-call}
requires a test suite to cover at least one call to a certain error method
(\mintinline{c}{__VERIFIER_error} or \mintinline{c}{reach_error}).
Criterion~\benchProperty{coverage-branches}
requires a test suite to cover all program branches.
For all benchmark~tasks with coverage criterion~\benchProperty{coverage-error-call}
it is known that at least one error-method call is reachable in the program;
i.e., the criterion can always be fulfilled.
For benchmark~tasks with coverage criterion~\benchProperty{coverage-branches},
it is not known whether all program branches are reachable.
This means that there may be tasks where coverage can not reach 100\,\%.

\begin{figure}[t]
\begin{minted}{yaml}
format_version: '2.0'

input_files: 'aws_add_size_checked_harness.i'

properties:
  - property_file: ../properties/unreach-call.prp
    expected_verdict: true
  - property_file: ../properties/coverage-branches.prp

options:
  language: C
  data_model: LP64
\end{minted}
\caption{An sv-benchmarks task-definition file% that defines one verification~task
    %(\benchProperty{unreach-call}) and one test-generation~task (\benchProperty{coverage-branches})
}
    \label{fig:taskdefinition}
\end{figure}
Tasks in sv-benchmarks are defined by task-definition~files in the
\href{https://gitlab.com/sosy-lab/benchmarking/task-definition-format/-/blob/f235a18a57ac9c18597c299d9d031cd0451c5bc5/README.md}{YAML format}~\cite{TESTCOMP21}.
\Cref{fig:taskdefinition}~shows an example task-definition~file that defines two tasks
with program \href{https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/ad265d07a04053bfcd80626ac1d30cec2d1c254e/c/aws-c-common/aws_add_size_checked_harness.i}{\mintinline{yaml}{'aws_add_size_checked_harness.i'}}:
One verification~task with property \benchProperty{unreach-call}
(which is true; i.e., no call to the error method is reachable),
and one test-generation~task with coverage~criterion \benchProperty{coverage-branches}.

Benchmark~tasks are grouped into categories.
For example,
category \href{https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/ad265d07a04053bfcd80626ac1d30cec2d1c254e/c/ReachSafety-BitVectors.set}{\benchCategory{ReachSafety-BitVectors}}
contains benchmark~tasks that require a verifier to reason about bitwise operations
(e.g., \mintinline{c}{x << 2} or \mintinline{c}{z = x ^ y})
and category \href{https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/blob/ad265d07a04053bfcd80626ac1d30cec2d1c254e/c/SoftwareSystems-AWS-C-Common-ReachSafety.set}{\benchCategory{SoftwareSystems-AWS-C-Common-ReachSafety}}
contains benchmark~tasks from the
AWS C~Common software~library\footnote{\url{https://github.com/awslabs/aws-c-common}}.

Each benchmark program uses methods
\texttt{X __VERIFIER_nondet_X()} to introduce
new non-deterministic values, where \mintinline{c}{X}
is a primitive data~type of C.
These methods are declared, but not implemented. Their meaning is implied
through the \svcomp and \testcomp rules.
Example methods are
\texttt{int __VERIFIER_nondet_int()} and
\texttt{float __VERIFIER_nondet_float()}.


%\newpage
\paragraph{Computing Resources}

In our experiments, we limit each tool~execution on a benchmark~task
to the same limits as \svcomp and \testcomp:
\SI{900}{\second} of CPU~time
and 15\,GB of memory~(RAM).
Experiments are distributed on a cluster of
equally configured machines with Intel processors%
---the latest iteration of the cluster
uses 168~machines
from~2015.
Each machine runs Ubuntu~Linux,
has a single 8~core Intel~Xeon~E3-1230~v5 CPU with \SI{3.40}{\GHz},
and 32\,GB of memory.
This computing power is similar to today's consumer machines.

%The cluster allows us to
%perform large-scale experiments to get representative data,
%This computing power reflects
%the to-be-expected performance of verification~tools
%when they are run on users' personal~computers.


\paragraph{Relevant Measurements}

In our evaluations, we often use two measures to compare
efficiency and effectiveness: CPU~time
and the overall number of results.

In contrast to the wall~time, the
CPU~time is the process~time the CPU actually spent computing.
This measurement is agnostic of slow I/O operations
and the number of CPU~cores used by a tool.
For example, if 4~CPU~cores work
\SI{5}{\second} each on a task
in perfect parallelism,
the wall~time is \SI{5}{\second},
but the CPU~time is \SI{20}{\second} ($4 * \SI{5}{\second}$).
If a single CPU~core waits
\SI{9}{\second} for I/O and then
computes for \SI{1}{\second}, the wall~time is \SI{10}{\second},
but the CPU~time is \SI{1}{\second}.
Thus, the CPU~time
is useful to measure the efficiency of a tool:
the actual computing effort the tool requires to perform a task.


%\subparagraph{Number of correct results}

For verification~tasks, we differentiate between
the following verification~results:
\begin{itemize}\firmlist
  \item Correctly solved safe~task (correct~proof).
    The tool successfully claims the program to be correct
    with regards to the property.
  \item Correctly solved unsafe~task (correct~alarm).
    The tool successfully claims the program to be incorrect
    with regards to the property.
  \item Incorrectly solved safe~task (incorrect~proof).
    The tool claims the program to be correct with regards to the property,
    but it is actually incorrect.
  \item Incorrectly solved unsafe~task (incorrect~alarm).
    The tool claims the program to be incorrect with regards to the property,
    but it is actually correct.
  \item Unknown result.
    The tool did not reach a verdict.
\end{itemize}

The number of correct results reflects the effectiveness of a tool,
while the number of incorrect results reflects its
%soundness and
imprecision.

For test-generation~tasks, we get the effectiveness of a tool
through the achieved coverage of the generated test-suite
(measured with \testcov, \cref{apx:ASE19}).

Other measures for performance~comparisons are wall~time (already mentioned),
memory~usage, and energy~consumption.
We focus on run~time to measure efficiency instead of memory~usage,
because, if our approaches fail to solve a verification~task,
this is almost exclusively because they reach the time~limit.
Energy~consumption expresses the amount
of energy (in kJ) the computation of a task requires.
While this correlates with run~time, there is no strict relation between the two.
Thus, energy~consumption would be a valuable supplement to run~time,
but reliable measurement has only become possible lately~\cite{CPUENERGYMETER, SVCOMP19}.
To achieve reliable measurements of resource-consumption, we use \benchexec%
\footnote{\url{https://github.com/sosy-lab/benchexec/}}~\cite{Benchmarking-STTT}.