
\paragraph{Program Properties}

Programs have liveness properties and safety properties~\cite{HBMC-TemporalLogic}.
%In general, it is not computable~\cite{Turing37}
%whether a given program fulfills a given property.
A liveness property requires that a program can always reach a wanted state.
A safety property requires that a program never reaches an
unwanted state.
In this work, we focus on verification techniques for safety properties.
From now on, we use the terms \emph{property} and \emph{safety property}
interchangeably.


\begin{figure}
  \begin{minipage}{.48\linewidth}
    \centering
    \begin{tikzpicture}
      \node[specNode, initial] (entry) {$\symstateInit$};
      \node[specNodeError, right = 3.5cm of entry] (error)  {$\symstateError$};

      \path (entry) edge[specEdge, bend left=10] node[above, specLabel] {$(\cdot, \texttt{reach_error()}, \cdot)$} (error);
      \path (entry) edge[specEdge, loop above] node[above, specLabel] {\smaller\textit{otherwise}} (entry);
    \end{tikzpicture}
    \vspace{2mm}

    \caption[%
    Observer automaton for the safety property that
    reach\textunderscore{}error() may never be called%
    ]{%
    Observer automaton for the safety property
    that \mintinline{c}{reach_error()}
    may never be called%
    }
    \label{fig:spec}
  \end{minipage}\hfill%
  \begin{minipage}{.48\linewidth}
    \centering
    \begin{tikzpicture}
      \node[specNode, initial] (entry) {$\symstateInit$};
      \node[specNodeError, right = 2cm of entry] (error)  {$\symstateError$};

      \path (entry) edge[specEdge, bend left=20] node[above, specLabel] {$(\cdot, \cdot, \symloc_{4})$} (error);
      \path (entry) edge[specEdge, loop above] node[above, specLabel] {\smaller\textit{otherwise}} (entry);
    \end{tikzpicture}
    \vspace{2mm}

    \caption{Observer automaton for the safety property that program location~$\symloc_{4}$ may never be reached}
    \label{fig:spec4}
  \end{minipage}
\end{figure}

We can express safety properties as finite automata,
called \emph{observer automata}.
For a program $\symCfa = (\symLocs, \symEdges, \symloc_0)$,
an observer automaton~%
$\symObserver_{\symCfa} =
  (\symStates, 2^\symEdges, \symTransitions, \symstateInit, \symAccepting)$
consists of a set~$\symStates$ of states,
the alphabet~$2^\symEdges$ of sets of~CFA~edges,
transitions~$\symTransitions : Q \times 2^\symEdges \times Q$,
an initial~state~$\symstateInit$,
and a set~$\symAccepting \subseteq \symStates$ of accepting states.
Accepting states represent violated properties%
%---i.e.,
%if there exists an accepting run through~$\symObserver_{\symCfa}$,
%program~$\symCfa$ violates the property
.
%
%Program~$\symCfa$
%violates the property of~$\symObserver_{\symCfa}$,
%if there is a feasible run through~$\symCfa$
%whose sequence of CFA~edges is an accepting~input
%for~$\symObserver_{\symCfa}$.
%
\Cref{fig:spec} shows the safety property that we focus on in most of our work:
If \mintinline{c}{reach_error()} is called at any point during program execution,
the property is violated.

Any (set of) coverage~goals can be expressed as safety property.
For example, for coverage goal~\fqlcode{@}$\symloc_4$, the corresponding safety property
is that the program may never reach $\symloc_4$ (\cref{fig:spec4}).
We exploit this relation in \cref{apx:ATVA19} to make formal verifiers
target branch coverage.

\paragraph{Program-State Reachability}
% Well visible for different kinds of color-blindness
%\definecolor{colorl1}{HTML}{FFF200}
%\definecolor{colorl2}{HTML}{ABD69C}
%\definecolor{colorl3}{HTML}{F7941E}
%\definecolor{colorl4}{HTML}{008FD5}
%\definecolor{colorl5}{HTML}{006F45}
%\definecolor{colorl7}{HTML}{741472}
%\definecolor{colorl10}{HTML}{cccccc}
% 
%\definecolor{colorl1}{HTML}{baf3ff}
%\definecolor{colorl2}{HTML}{bad1ff}
%\definecolor{colorl3}{HTML}{c6baff}
%\definecolor{colorl4}{HTML}{e9baff}
%\definecolor{colorl5}{HTML}{ffbaf3}
%\definecolor{colorl7}{HTML}{ffbad1}
%\definecolor{colorl10}{HTML}{ffbad1}
%\definecolor{colorl2}{HTML}{99b9ff}
%\definecolor{colorl4}{HTML}{df99ff}
%\definecolor{colorl7}{HTML}{ff99b9}
%
\definecolor{colorl1}{HTML}{99ecff}
\definecolor{colorl2}{HTML}{ac99ff}
\definecolor{colorl3}{HTML}{ff99ec}
\definecolor{colorl4}{HTML}{ffac99}
\definecolor{colorl7}{HTML}{ecff99}
\definecolor{colorl5}{HTML}{99ffdf}
\definecolor{colorl10}{HTML}{b9ff99}
\begin{figure}[t]
  \parbox{.48\linewidth}{%
    \scalebox{0.68}{
      \begin{tikzpicture}[node distance=5mm,
          flow/.append style={draw=black!90!white}]
        \node[concrete state, fill=colorl1] (l1) {l_1, x \mapsto 0, n \mapsto 0};
        \coordinate[above left = 3mm of l1] (start-init);
        \node[left = of l1] (l1-l-dots) {\Large \ldots};
        \node[right = of l1] (l1-r-dots) {\Large \ldots};
        \node[concrete state, below = of l1, fill=colorl2] (l2) {l_2, x \mapsto 0, n \mapsto 0};
        \node[concrete state, below = of l2  , fill=colorl3] (l3-0) {l_3, x \mapsto 0, n \mapsto 0};
        \node[concrete state, left = of l3-0 , fill=colorl3] (l3--1) {l_3, x \mapsto 0, n \mapsto -1};
        \node[concrete state, right = of l3-0, fill=colorl3] (l3-1) {l_3, x \mapsto 0, n \mapsto 1};
        \node[concrete state, right = of l3-1, fill=colorl3] (l3-2) {l_3, x \mapsto 0, n \mapsto 2};
        \node[concrete state, right = of l3-2, fill=colorl3] (l3-3) {l_3, x \mapsto 0, n \mapsto 3};
        \node[concrete state, right = of l3-3, fill=colorl3] (l3-4) {l_3, x \mapsto 0, n \mapsto 4};

        \node[concrete state, below = of l3-0, fill=colorl7] (l7-0) {l_7, x \mapsto 0, n \mapsto 0};
        \node[concrete state, below = of l3--1, fill=colorl7] (l7--1) {l_7, x \mapsto 0, n \mapsto -1};

        \node[concrete state, below = of l7-0 , fill=colorl10] (l10-0) {l_{10}, x \mapsto 0, n \mapsto 0};
        \node[concrete state, below = of l7--1, fill=colorl10] (l10--1) {l_{10}, x \mapsto 0, n \mapsto -1};

        \node[concrete state, below = of l3-1, fill=colorl4] (l4-1) {l_4, x \mapsto 0, n \mapsto 1};
        \node[concrete state, below = of l4-1, fill=colorl5] (l5-1) {l_5, x \mapsto 2, n \mapsto 1};
        \node[concrete state, below = of l5-1, fill=colorl3] (l3-1-cyc-2) {l_3, x \mapsto 2, n \mapsto 1};
        \node[concrete state, below = of l3-1-cyc-2, fill=colorl7] (l7-1-cyc-2) {l_7, x \mapsto 2, n \mapsto 1};

        \node[concrete state, below = of l7-1-cyc-2, fill=colorl10] (l10-1-cyc-2) {l_{10}, x \mapsto 2, n \mapsto 1};
        \node[concrete state, below = of l3-2, fill=colorl4] (l4-2) {l_4, x \mapsto 0, n \mapsto 2};
        \node[concrete state, below = of l4-2, fill=colorl5] (l5-2) {l_5, x \mapsto 2, n \mapsto 2};
        \node[concrete state, below = of l5-2, fill=colorl3] (l3-2-cyc-2) {l_3, x \mapsto 2, n \mapsto 2};
        \node[concrete state, below = of l3-2-cyc-2, fill=colorl7] (l7-2-cyc-2) {l_7, x \mapsto 2, n \mapsto 2};
        \node[concrete state, below = of l7-2-cyc-2, fill=colorl10] (l10-2-cyc-2) {l_{10}, x \mapsto 2, n \mapsto 2};

        \node[concrete state, below = of l3-3, fill=colorl4] (l4-3) {l_4, x \mapsto 0, n \mapsto 3};
        \node[concrete state, below = of l4-3, fill=colorl5] (l5-3) {l_5, x \mapsto 2, n \mapsto 3};
        \node[concrete state, below = of l5-3, fill=colorl3] (l3-3-cyc-2) {l_3, x \mapsto 2, n \mapsto 3};
        \node[concrete state, below = of l3-3-cyc-2, fill=colorl4] (l4-3-cyc-2) {l_4, x \mapsto 0, n \mapsto 3};
        \node[concrete state, below = of l4-3-cyc-2, fill=colorl5] (l5-3-cyc-2) {l_5, x \mapsto 4, n \mapsto 3};
        \node[concrete state, below = of l5-3-cyc-2, fill=colorl3] (l3-3-cyc-3) {l_3, x \mapsto 4, n \mapsto 3};
        \node[concrete state, below = of l3-3-cyc-3, fill=colorl7] (l7-3-cyc-3) {l_7, x \mapsto 4, n \mapsto 3};
        \node[concrete state, below = of l7-3-cyc-3, fill=colorl10] (l10-3-cyc-3) {l_{10}, x \mapsto 4, n \mapsto 3};

        \node[concrete state, below = of l3-4, fill=colorl4] (l4-4) {l_4, x \mapsto 0, n \mapsto 4};
        \node[concrete state, below = of l4-4, fill=colorl5] (l5-4) {l_5, x \mapsto 2, n \mapsto 4};
        \node[concrete state, below = of l5-4, fill=colorl3] (l3-4-cyc-2) {l_3, x \mapsto 2, n \mapsto 4};
        \node[concrete state, below = of l3-4-cyc-2, fill=colorl4] (l4-4-cyc-2) {l_4, x \mapsto 0, n \mapsto 4};
        \node[concrete state, below = of l4-4-cyc-2, fill=colorl5] (l5-4-cyc-2) {l_5, x \mapsto 4, n \mapsto 4};
        \node[concrete state, below = of l5-4-cyc-2, fill=colorl3] (l3-4-cyc-3) {l_3, x \mapsto 4, n \mapsto 4};
        \node[concrete state, below = of l3-4-cyc-3, fill=colorl7] (l7-4-cyc-3) {l_7, x \mapsto 4, n \mapsto 4};
        \node[concrete state, below = of l7-4-cyc-3, fill=colorl10] (l10-4-cyc-3) {l_{10}, x \mapsto 4, n \mapsto 4};

        \node[right = of l3-4] (l3-dots) {\Large \vdots};
        %\node[left = of l3--1] (l3--dots) {\ldots};
        \node[left = of l3--1] (l7--dots) {\Large \vdots};
        \node[left = of l10--1, xshift=-2pt] (l10--dots) {\Large \ldots};
        \node[right = of l5-4, xshift=2pt] (l5-dots) {\Large \ldots};
        %\node[left = of l10--1] (l10--dots) {\ldots};

        \path
        (start-init) edge[flow] (l1)
        (l1) edge[flow] (l2)
        (l2) edge[flow] (l3-0)
        (l2) edge[flow] (l3--1)
        (l2) edge[flow] (l3-1)
        (l2) edge[flow] (l3-2)
        (l2) edge[flow, bend left=5] (l3-3)
        (l2) edge[flow, bend left=8] (l3-4)
        (l2) edge[flow, bend left=15] (l3-dots |- l3-4.north)
        (l2) edge[flow, bend right=15] (l7--dots |- l3--1.north)
        (l3-0) edge[flow] (l7-0)
        (l7-0) edge[flow] (l10-0)
        (l3--1) edge[flow] (l7--1)
        (l7--1) edge[flow] (l10--1)
        (l3-1) edge[flow] (l4-1)
        (l4-1) edge[flow] (l5-1)
        (l5-1) edge[flow] (l3-1-cyc-2)
        (l3-1-cyc-2) edge[flow] (l7-1-cyc-2)
        (l7-1-cyc-2) edge[flow] (l10-1-cyc-2)

        (l3-2) edge[flow] (l4-2)
        (l4-2) edge[flow] (l5-2)
        (l5-2) edge[flow] (l3-2-cyc-2)
        (l3-2-cyc-2) edge[flow] (l7-2-cyc-2)
        (l7-2-cyc-2) edge[flow] (l10-2-cyc-2)

        (l3-3) edge[flow] (l4-3)
        (l4-3) edge[flow] (l5-3)
        (l5-3) edge[flow] (l3-3-cyc-2)
        (l3-3-cyc-2) edge[flow] (l4-3-cyc-2)
        (l4-3-cyc-2) edge[flow] (l5-3-cyc-2)
        (l5-3-cyc-2) edge[flow] (l3-3-cyc-3)
        (l3-3-cyc-3) edge[flow] (l7-3-cyc-3)
        (l7-3-cyc-3) edge[flow] (l10-3-cyc-3)

        (l3-4) edge[flow] (l4-4)
        (l4-4) edge[flow] (l5-4)
        (l5-4) edge[flow] (l3-4-cyc-2)
        (l3-4-cyc-2) edge[flow] (l4-4-cyc-2)
        (l4-4-cyc-2) edge[flow] (l5-4-cyc-2)
        (l5-4-cyc-2) edge[flow] (l3-4-cyc-3)
        (l3-4-cyc-3) edge[flow] (l7-4-cyc-3)
        (l7-4-cyc-3) edge[flow] (l10-4-cyc-3)

        ;
      \end{tikzpicture}
    }
  }\vspace{-4cm}\\
  \parbox{0.35\textwidth}{%
    \fboxsep3pt%
    \colorbox{colorLightGray}{%
      \centering
      \scalebox{0.75}{%
        \begin{tikzpicture}[node distance=5mm]
          \node[program state, fill=colorl1] (l1) {l_1, true};
          \coordinate[above left = 3mm of l1] (start-init);
          \node[abstract state, below = of l1, fill=colorl2] (l2) {l_2, x \myMod 2 = 0};
          \node[abstract state, below = of l2, fill=colorl3] (l3-0) {l_3, x \myMod 2 = 0};

          \node[abstract state, below left = 8mm and -9mm of l3-0, fill=colorl4] (l4) {l_4, x \myMod 2 = 0};
          \node[abstract state, below = of l4, fill=colorl5] (l5) {l_5, x \myMod 2 = 0};

          \node[abstract state, below right = 8mm and -9mm of l3-0, fill=colorl7] (l7) {l_7, x \myMod 2 = 0};
          \node[abstract state, below = of l7, fill=colorl10] (l10) {l_{10}, true};

          % to increase the padding of the surrounding colorbox
          \node[below = 1mm of l10] (placeholder) {};

          \path
          (start-init) edge[flow] (l1)
          (l1) edge[flow] node[right, cfaLabel] {x = 0} (l2)
          (l2) edge[flow] node[right, cfaLabel] {n = nondet()} (l3-0)
          (l3-0) edge[flow] node[left, cfaLabel, yshift=2pt] {[x < n]} (l4)
          (l3-0) edge[flow] node[right, cfaLabel, yshift=2pt] {[!(x < n)]} (l7)
          (l4) edge[flow] node[left, cfaLabel] {x += 2} (l5)
          (l5.east) edge[flow,bend right=25] node[right, cfaLabel] {} (l3-0.south)
          (l7) edge[flow] node[right, cfaLabel] {[x \% 2 = 0]} (l10)
          ;
        \end{tikzpicture}
      }
    }
  }\vspace{0.4\baselineskip}\\%
  \null\hfill\parbox[t]{0.34\textwidth}{%
    \caption{Exemplary program~abstraction for~\cref{fig:concrete-states}}
    \label{fig:abstract-states}
  }%
  \null\hfill\parbox[t]{0.65\textwidth}{%
    \caption{Concrete state~space of \cref{fig:cfa}}
    \label{fig:concrete-states}
  }
\end{figure}

To show that a program~$\symCfa$ violates a safety property
represented by $\symObserver_{\symCfa}$,
verification techniques search for a counterexample to the property;
i.e., a program execution
whose sequence of CFA~edges
are an accepted input to~$\symObserver_{\symCfa}$.
Such a sequence of CFA~edges can also be implied by a test input.
%
Analogous, to show that a program fulfills a safety property,
verification techniques have to show
that there exists no violating program execution.
%
\Cref{fig:concrete-states} shows an excerpt of the infinite, concrete program-state~space
of \cref{fig:cfa}.
The concrete states are structured according
to their predecessor-successor relation of a potential program~execution.
At the program entry~$\symloc_1$, there is an infinite number of possible concrete
states for the still-unassigned program variables~\mintinline{c}{x} and~\mintinline{c}{n}.
We restrict our excerpt to the program-state~space for $x \mapsto 0, n \mapsto 0$.
At $\symloc_3$, because input~variable~\mintinline{c}{n} is not constrained,
it may take any value. This means
that the loop in the program may be unrolled an indefinite amount of times,
and that there is an infinite number of possible concrete states.

Since testing can only prove a program safe
through explicitly enumerating
% if only 'relevant program executions' are enumerated,
% it is also necessary to show that only these are relevant.
all feasible program executions like this, it does not scale.

\paragraph{Formal Verification}

\begin{figure}[t]
  \parbox[t]{0.48\linewidth}{%
    \begin{tikzpicture}[
        node distance=6mm,
        verdict/.append style={minimum width=3cm},
      ]
      \node[artifact program] (prog) {$\symCfa$};
      \node[artifact criterion, below = 6mm of prog] (property) {$\symproperty$};
      \coordinate (rect-start) at ($(prog.north west) + (-2mm, 2mm)$);

      \draw[dashed] (rect-start) rectangle ($(property.south east) + (2mm, -2mm)$);
      \node[below = 2mm of criterion, align=center, text width=2cm] {\footnotesize Verification Task};

      \coordinate (middle) at ($(prog.east)!0.5!(property.east)$);
      \node[actor, right = of middle] (verifier) {Formal Verifier};
      \coordinate[right = of verifier] (anchor-right);
      %\node[verdict bubble, fill=colorUnknown] (yellow) {\textbf{?}};
      %\node[verdict bubble, above = 2mm of yellow, fill=colorTrue] (green) {\cmark};
      %\node[verdict bubble, below = 2mm of yellow, fill=colorFalse] (red) {\xmark};
      %\node[artifact, anchor=west]
      %    (witness)
      %    at (anchor-right |- property)
      %    {$\symwitness$};

      \begin{pgfonlayer}{background}
        \node[fill=colorFalse, minimum width=1.8cm, minimum height=1.8cm,
          text depth=1cm, font=\small, anchor=west,
        ] (incorrect) at (anchor-right) {$\symCfa \not\models \symproperty$};
        \node[fill=colorUnknown, minimum width=1.8cm, minimum height=1cm,
          font=\small,
          below = 4mm of incorrect
        ]
        (unknown) {Unknown};
        \node[fill=colorTrue, minimum width=1.8cm, minimum height=1.8cm,
          text depth=1cm, font=\small,
          above = 4mm of incorrect] (correct) {$\symCfa \models \symproperty$};
      \end{pgfonlayer}

      %\node[below = 1mm of red.south, anchor=north] (verdict-label) {Verdict};

      \node[artifact, scale=.7, above = 2mm of correct.south] (proofwitness) {$\symcorrectnesswitness$};
      \node[artifact, scale=.7, above = 2mm of incorrect.south] (violationwitness) {$\symviolationwitness$};

      \coordinate[left = 4mm of incorrect] (crossing);


      \path
      (prog) edge[flow] (verifier)
      (property) edge[flow] (verifier)
      %(verifier) edge[flow] (witness)
      %(verifier) edge[flow] (crossing)
      ;
      \path[draw, thick]
      (verifier) -- (crossing) edge[flow] (incorrect)
      (crossing) edge[flow] (correct.south west -| correct.west)
      (crossing) edge[flow] (unknown.north west -| unknown.west)
      ;

    \end{tikzpicture}
  }\hfill%
  \parbox[t]{0.48\linewidth}{%
    \begin{tikzpicture}[
        node distance=6mm,
        verdict/.append style={minimum width=3cm},
      ]
      \node[artifact program] (prog) {$\symCfa$};
      \node[artifact criterion, below = 6mm of prog] (property) {$\symproperty$};

      %\coordinate (rect-start) at ($(prog.north west) + (-2mm, 2mm)$);
      %\draw[dashed] (rect-start) rectangle ($(property.south east) + (2mm, -2mm)$);
      %\node[below = 2mm of criterion, align=center, text width=2cm] {\footnotesize Verification Task};

      \node[artifact violation witness, below = 6mm of property] (witness) {$\symviolationwitness$};

      \coordinate (middle) at (property.east);
      \node[actor, right = of middle] (verifier) {Witness Validator};
      \coordinate[right = of verifier] (anchor-right);
      %\node[verdict bubble, fill=colorUnknown] (yellow) {\textbf{?}};
      %\node[verdict bubble, above = 2mm of yellow, fill=colorTrue] (green) {\cmark};
      %\node[verdict bubble, below = 2mm of yellow, fill=colorFalse] (red) {\xmark};
      %\node[artifact, anchor=west]
      %    (witness)
      %    at (anchor-right |- property)
      %    {$\symwitness$};


      \begin{pgfonlayer}{background}
        \node[fill=colorUnknown, minimum width=2.4cm, minimum height=1cm, anchor=west,
          below = 4mm of anchor-right, anchor=north west, font=\small]
        (unknown) {Unconfirmed};
        \node[fill=colorConfirmed, minimum width=2.4cm, minimum height=1.8cm,
          text depth=1cm, font=\small,
          above = 4mm of unknown] (correct) {Confirmed};
      \end{pgfonlayer}

      %\node[below = 1mm of red.south, anchor=north] (verdict-label) {Verdict};

      \node[artifact violation witness, scale=.7, above = 2mm of correct.south] (correctnesswitness) {$\symviolationwitness$};

      \coordinate[left = 4mm of anchor-right] (crossing);

      \path
      (prog) edge[flow] (verifier)
      (property) edge[flow] (verifier)
      (witness) edge[flow] (verifier)
      %(verifier) edge[flow] (witness)
      %(verifier) edge[flow] (crossing)
      ;
      \path[draw, thick]
      (verifier) -- (crossing) edge[flow] (unknown.north west -| unknown.west)
      (crossing) edge[flow] (correct)
      ;

    \end{tikzpicture}
  }%
  \vspace{0.5\onelineskip}
  \null\hfill\parbox[t]{0.48\linewidth}{%
    \caption{Verification of program~$\symCfa$
      with regards to property~$\symproperty$}
    \label{fig:verifier}
  }%
  \null\hfill\parbox[t]{0.48\linewidth}{%
    \caption[Validation of violation~witness~$\symviolationwitness$]{%
    Validation of violation~witness~$\symviolationwitness$
      (analogous for correctness~witnesses)}
    \label{fig:validator}
  }
\end{figure}

Formal verification~\cite{HBMC-book}
checks whether a program~$\symCfa$ fulfills a property~$\symproperty$,
formally $\symCfa \models \symproperty$.
If $\symCfa \models \symproperty$,
we say that
$\symCfa$ is \emph{correct with regards to $\symproperty$}.
If $\symCfa \not\models \symproperty$,
we say that
$\symCfa$ is \emph{incorrect with regards to $\symproperty$}.

Most formal~verification~techniques~\cite{SVCOMP22}
construct correctness~proofs through abstraction~\cite{AbstractInterpretation}:
they use an abstraction to the concrete program
and exhaustively explore that abstraction's state~space.
If no state violates the program~property, the program is safe
with regards to the checked property.
\Cref{fig:abstract-states} shows an abstract state-space exploration
for \cref{fig:cfa}
and the property that \mintinline{c}{reach_error} is never called.
The exploration uses a simplified view on
predicate~abstraction~\cite{SLAM2001}
with predicates~$true$ and $x \myMod 2 = 0$.
It represents sets of concrete states through
the current program~location~$\symloc \in \symLocs$
and either constraint $true$ (any concrete state at $\symloc$)
or constraint $x \myMod 2 = 0$ (any concrete state at~$\symloc$
whose variable~assignment fulfills $x \myMod 2 = 0$).
Formally, the concrete state~space represented by such an abstract~state is
$\llbracket (l, \sympredicate) \rrbracket  = \{(l, \sigma) \in \symConcretes\
  |\ \sigma \models \sympredicate\}$.

The color of each abstract~state in \cref{fig:abstract-states}
signals the set of concrete~states of \cref{fig:concrete-states}
that it represents.
Because the concrete value
of \mintinline{c}{n} is arbitrary in all abstract states,
and the value of \mintinline{c}{x}
is known to be $x \myMod 2 = 0$ at $\symloc_7$,
the constructed abstract state~space is finite:
For each program location, it only requires a single abstract state
that represents all concrete states possible at that location.
Thanks to the predicate,
the program~abstraction can still proof that the CFA~edge~$(\symloc_7, \texttt{[!(x \% 2 = 0)]}, \symloc_9)$
that leads to \mintinline{c}{reach_error()}
is never feasible.

Using a suitable program~abstraction is essential
for reliably proving a program safe.
Different approaches exist for this (e.g., \cite{BMCJournal,AbstractionsFromProofs,CraigVsNewton,Octagon,King76})
with different strengths and weaknesses.
\Cref{sec:cooperative-verification} works towards combining approaches
to profit from their strengths and mitigate their weaknesses.

\paragraph{Verification-Result Witnesses}

To increase the confidence in verification results,
a formal~verifier~(\cref{fig:verifier})
produces a verification-result witness:
For $\symCfa \models \symproperty$
a correctness~witness~$\symcorrectnesswitness$,
and
for $\symCfa \not\models \symproperty$
a violation~witness~$\symviolationwitness$.
These witnesses can be checked by independent \emph{witness~validators}~\cite{WitnessesACM}.
A witness validator~(\cref{fig:validator})
tries to reconstruct the verification~result
from program~$\symCfa$ and property~$\symproperty$ with the help of the witness.
%---this means that verification~results are potentially easier to confirm
%with high-quality witnesses
If this is successful, the verification~result is \emph{confirmed}.
If it is not successful, the verification~result stays \emph{unconfirmed}.
The verification~result is not rejected per se,
because the reason for a missing confirmation
is not necessarily a wrong verdict; for example,
validator \cpawtt~(\cref{apx:TAP18})
can only validate violation~witnesses that specify all input values.
A witness~validator can provide additional information
to a confirmed verification~result through a more detailed witness,
called \emph{testification}~\cite{Witnesses}.
We use testification for violation~witnesses in~\cref{apx:TAP18}
to generate from imprecise witnesses
more precise witnesses that contain all information
that is required to create test~inputs.%for subsequent test~generation.

%\subparagraph{Violation Witness}
A witness consists of two parts:
metadata about the verification~task,
and a witness automaton with information that
helps to recompute the claimed verification~result.
The type of witness~automaton differs for violation~witness and correctness~witness.
This work focuses on violation~witnesses.

For a program~$\symCfa = (\symLocs, \symEdges, \symloc_0)$,
a source-code guard~$\symSourceCodeGuard \subseteq \symEdges$ is a set of CFA~edges.
A state-space guard~$\symStateSpaceGuard \subseteq \symConcretes$ is a set of
concrete program~states.
Set~$\symSetStateSpaceGuard$ contains all possible state-space guards.
Violation-witness~automata use
source-code guards and state-space guards
%at state transitions~$(\symWitstate, (\symSourceCodeGuard, \symStateSpaceGuard), \symWitstate')$
to restrict the program-state space to
an (ideally small)~subset that contains the claimed property~violation.

For a program~$\symCfa = (\symLocs, \symEdges, \symloc_{0})$,
a violation-witness~automaton~$\symWitnessAutomaton
  = (\symWitStates, \symWitAlphabet, \symWitTransitions, \symWitstateInit, \symWitAcceptingStates)$
is a finite automaton that consists of
a set~$\symWitStates$ of states,
alphabet~$\symWitAlphabet \subseteq 2^\symEdges \times \symSetStateSpaceGuard$ of source-code guards
and state-space guards,
transitions~$\symWitTransitions : \symWitStates \times \symWitAlphabet \times \symWitStates$,
initial~state~$\symWitstateInit \in \symWitStates$,
and accepting states~$\symWitAcceptingStates \subseteq \symWitStates$
that represent that the claimed property~violation is reached.
For a transition $(\symWitstate, (\symSourceCodeGuard, \symStateSpaceGuard), \symWitstate')$,
the source-code~guard~$\symSourceCodeGuard$
allows the transition from $\symWitstate$ to $\symWitstate'$
if the next control-flow edge is in~$\symSourceCodeGuard$.
After transitioning to~$\symWitstate'$,
state-space~guard~$\symStateSpaceGuard$
restricts the set of concrete program~states to $\symStateSpaceGuard$.

In some of our work, we use violation~witnesses with only source-code guards.
In consequence, we omit state-space guards
in these representations of violation-witness~automata
---at each edge, the state-space guard is the trivial guard~$\symConcretes$.
%
%
%In contrast to proof-carrying-code~\cite{PCC}
%and certifying~model~checkers~\cite{Namjoshi01},
%a witness may be a \emph{partial} proof.

%\begin{figure}[t]
%    \parbox[t]{0.48\linewidth}{%
%      \begin{tikzpicture}[node distance=7mm]
%        \node[witnessNode, initial] (0) {$q_0$};
%        \node[witnessNode, below = of 0] (1) {$q_1$};
%        \node[witnessNode, below = of 1] (2) {$q_2$};
%        \node[witnessNode, accepting, draw=colorViolation, below = of 2] (err) {$q_{err}$};
%
%        \path (0) edge[witnessEdge] node[right, witnessLabel] {line 2:\\y = 1} (1)
%        (1) edge[witnessEdge] node[right, witnessLabel] {line 3, cond-true} (2)
%        (2) edge[witnessEdge] node[right, witnessLabel] {line 5, cond-false} (err);
%        \path (0) edge[witnessEdge, loop right] node[right, witnessLabelOw] {o/w} (0)
%        (1) edge[witnessEdge, loop right] node[right, witnessLabelOw] {o/w} (1)
%        (2) edge[witnessEdge, loop right] node[right, witnessLabelOw] {o/w} (2);
%      \end{tikzpicture}
%    }\hfill%
%    \parbox[t]{0.48\linewidth}{%
%      \begin{tikzpicture}[node distance=7mm]
%        \node[witnessNode, initial] (0) {$q_0$};
%        \node[witnessNode, below = of 0] (1) {$q_1$};
%        \node[witnessInvariantNode, below = of 1] (inv) {$q_{2}$\\$x \leq 2 * (x / 2)$};
%        \node[witnessNode, below left = of inv] (3) {$q_{4}$};
%        \node[witnessNode, below right = of inv] (4) {$q_{3}$};
%
%        \path (0) edge[witnessEdge] node[right, witnessLabel] {line 2} (1)
%        (1) edge[witnessEdge] node[right, witnessLabel, pos=0.3] {line 3, cond-true} (inv)
%        (inv) edge[witnessEdge] node[left, yshift=10pt, witnessLabel, align=right] {line 4,\\ cond-false} (3)
%        (inv) edge[witnessEdge] node[right, yshift=10pt, witnessLabel] {line 4,\\ cond-true} (4)
%        (4) edge[witnessEdge, bend left=45] node[below, pos=0.4, yshift=-2pt, witnessLabel] {line 5} (inv);
%        \path (0) edge[witnessEdge, loop right] node[right, witnessLabelOw] {o/w} (0)
%        (1) edge[witnessEdge, loop right] node[right, witnessLabelOw] {o/w} (1)
%        (inv) edge[witnessEdge, loop, in=10, out=30, min distance=1mm, looseness=5] node[right, witnessLabelOw] {o/w} (inv)
%        (3) edge[witnessEdge, loop below] node[right, witnessLabelOw] {o/w} (3)
%        (4) edge[witnessEdge, loop below] node[right, witnessLabelOw] {o/w} (4);
%      \end{tikzpicture}
%    }%
%    \vspace{0.5\onelineskip}
%    \null\hfill\parbox[t]{0.48\linewidth}{%
%        \caption{Example violation-witness automaton}
%        \label{fig:violationwitness}
%    }%
%    \null\hfill\parbox[t]{0.48\linewidth}{%
%        \caption{Example correctness-witness automaton}
%        \label{fig:correctnesswitness}
%    }
%\end{figure}
%
%\subparagraph{Correctness Witness}

%\subparagraph{Exchange format}
For storage and exchange between tools,
we use the exchange format%
\footnote{\url{https://github.com/sosy-lab/sv-witnesses}}%
~\cite{WitnessesACM}
for witnesses
that is based on XML/GraphML~\cite{GraphML}.
This format is supported by all \svcomp participants since \svcomp~2015~\cite{SVCOMP15}
and is---to the best of our knowledge---the only widely adopted exchange format for
verification-results that allows to encode semantic information for reasoning about the result.
% Other format:
% SARIF\footnote{\url{https://docs.oasis-open.org/sarif/sarif/v2.0/csprd02/sarif-v2.0-csprd02.html}}

%\paragraph{The International Competition on Software Verification}
%The International Competition on Software Verification (SV-COMP) helps establish
%common formats for verifiers, and is strongly related with sv-benchmarks,
%DESCRIPTION OF sv-benchmarks


\paragraph{Conditional Verification}
\input{background/cmc}


\paragraph{CPAchecker}

\cpachecker%
\footnote{\url{https://cpachecker.sosy-lab.org/}}%
~\cite{CPACHECKER} is a software-analysis framework
with a modular structure.
It consistently achieves good results at \svcomp,
is actively maintained,
and is designed for integration and combination of new algorithms.
\cpachecker supports witness~generation as a verifier,
violation-witness~\cite{Witnesses}
and correctness-witness~\cite{CorrectnessWitnesses} validation,
and conditional model checking~\cite{ConditionalModelChecking}.
For these reasons, we used it to implement multiple of our proposed concepts.



