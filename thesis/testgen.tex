\section{A Level Playing Field for Test-Generation Comparison}

Software testing is universal in software development,
but formal verification is hardly used.
%There are multiple reasons for this -- (let's not get into this)
%\marginpar{\scriptsize\raggedright\fullcite{HVC17}}
In the article~``\citetitle{HVC17}''~(\cref{apx:HVC17}),
we ignore potential other reasons for this, and
examine whether testing is actually better at finding bugs than
formal verification.

Formal verification is often seen as a means to prove the safety of software,
which requires large effort and expertise.
But our article shows that
the program~abstractions of formal verification~techniques
are effective at finding bugs, as well:
In some way, a formal verifier that finds a fitting abstraction to a program
is more precise than dynamic test~execution, because
actually irrelevant program parts are not explored.~\cite{HumbleProgrammer}

To show this, we compare the bug-finding capabilities
of existing state-of-the-art test~generators
and formal verifiers for C~programs.
We select six~test~generators
with different backgrounds:
two test~generators use formal-verification techniques tuned for testing~\cite{CpaTiger,FShell},
one test~generator uses symbolic execution~\cite{KLEE},
one test~generator uses concolic execution~\cite{CRESTPPC}, and
one test~generator uses random fuzz testing~\cite{AFLfast}.
As a baseline, we implement a plain random tester (an early version of \prtest~\cite{PRTEST19}).
%
As formal~verifiers, we select the four top-performers at finding
bugs in the International Competition on Software~Verification~2017 (\svcomp~2017)~\cite{SVCOMP17}.

\paragraph{Requirements for Reliable Comparison}
For reliable comparison, it is necessary
to have a well-defined benchmark-task~set in a format
that all tools understand.
In addition, all tools have to
use the same output~format for verification results,
so that results can be processed uniformly.

These conditions are met 
by formal~verifiers:
\svcomp~\cite{SVCOMP23}
establishes standardized formats~\cite{SVCOMP16}
for verification~tasks
and verification~results.
For verification tasks, it uses the sv-benchmarks%
\footnote{\url{https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/}}.
This benchmark~set is community-driven and constantly
expanding, contains explicitly annotated errors,
puts great effort in removing all undefined behavior in programs%
\footnote{\svcomp participants are incentivized to find undefined behavior
in programs to improve their tool's performance in the competition.},
and
structures benchmark~tasks in categories with different focus.
%
For verification~results, \svcomp uses the exchange format
for verification-result~witnesses~\cite{Witnesses}.
%
%Tool developers have to adhere to these standards
%when they participate in \svcomp.
%This allows a detailed and reliable comparison of formal~verifiers.

For test~generators for~C,
there existed no comparable standard before 
the first iteration of the International Competition on Software Testing~\cite{TESTCOMP19}.
To mitigate this, we built the framework~\tbf~\cite{HVC17}
that adjusts inputs to each test~generator's expected format,
and parses test~generators' different output formats
into a single unified structure.

\paragraph{Benchmark Set for Test Generation}
The research in testing C~programs
has no standard benchmark to compare against:
Many works~\cite{KLEE,CUTE,PARTI,CREST,DirectedSymExec} use
different real-world C~code, most prominently
the GNU coreutils\footnote{\url{https://www.gnu.org/software/coreutils/}}
and GNU grep\footnote{\url{https://www.gnu.org/software/grep/}}.
But this real-world software contains undefined behavior
that may be interpreted differently by different test~generators
and
it is unknown how many bugs exist at which locations.
%
To provide a well-defined alternative,
we turn the verification tasks of sv-benchmarks
into test-generation tasks.

Each of the selected test~generators requires the program to mark inputs
for which to generate test~input in different ways:
\begin{itemize}\tightlist
    \item \afl~\cite{AFLfast} requires a program~under~test to read test~input
            from a file in any way it likes. The file is passed to the program
            as command-line argument.
    \item \cpatiger~\cite{CpaTiger} uses function \mintinline{c}{input()}
            to mark input; for example \mintinline{c}{x = input()}.
    \item \crestppc~\cite{CRESTPPC}
            uses special C~macros \mintinline{c}{CREST_X(x)}
            with primitive~type~\mintinline{c}{X}
            to mark program variable~\mintinline{c}{x}
            of type \mintinline{c}{X} as input; for example \mintinline{c}{CREST_int(x)}.
    \item \fshell~\cite{FShell-Tool}
            assumes that any function without existing definition
            and any of C's scanf methods read input.
    \item \klee~\cite{KLEE} uses the function
            \mintinline{c}{klee_make_symbolic("x", &x, sizeof(x))}
            to mark program variable \mintinline{c}{x} as input.
    \item \prtest~\cite{PRTEST19} uses the sv-benchmarks methods.
            For example, the program statement \mintinline{c}{x = __VERIFIER_nondet_int()}
            introduces a new input value of type int.
\end{itemize}

Of the selected test~generators, only \fshell and \cpatiger
allow to specify a coverage~criterion. We specify branch~coverage.
\afl uses a coverage~criterion similar to condition~coverage,
\klee and \crestppc use condition~coverage.
\prtest does not consider any coverage~criterion, but indefinitely
generates new random tests.

\paragraph{Test-Suite Formats}
JUnit is a vastly popular framework for writing program tests for Java
and often used in test-generation~research~\cite{Evosuite,Randoop,JPF}.
But no comparably established framework exists for C.
Instead, test~generators for C produce test-suites in
almost arbitrary formats.
The test~generators that we consider use the following formats:
\begin{itemize}\tightlist
    \item \afl~\cite{AFLfast} stores each generated test as an individual file.
          The (binary) content of the file is the test input.
    \item \cpatiger~\cite{CpaTiger} stores all generated tests in a single file
            in a JSON-like structure.
    \item \crestppc~\cite{CRESTPPC} stores all generated tests in a single file.
          Each line in that file is an individual test with comma-separated input values.
    \item \fshell~\cite{FShell-Tool} stores all generated tests in a single file;
            in a format similar to \cpatiger, but not equal.
    \item \klee~\cite{KLEE} stores each generated test as an individual file,
            in a proprietary binary format.
            It ships a tool for both inspecting and executing the generated tests.
            This tool outputs a list of the name, size and value of each input.
    \item \prtest~\cite{PRTEST19} stores each generated test as an individual file.
            Each line in that file is one input value.
\end{itemize}

\paragraph{TBF}
Our tool~\tbf adds implementations for all methods \mintinline{c}{__VERIFIER_nondet_X()}
to match the test~generator's expectations.
For example, for \crestppc, \tbf defines:

\begin{minted}[frame=none]{c}
int __VERIFIER_nondet_int() {
  int __sym;
  CREST_int(__sym);
  return __sym;
}
\end{minted}

\tbf understands the six different output formats for test~suites,
converts them into a uniform internal structure,
and executes that the same way for each tester.

For execution, \tbf defines method \mintinline{c}{__VERIFIER_error}
in the program~under~test to make its calls easily observable:

\begin{minted}[frame=none]{c}
int __VERIFIER_error() {
  fprintf(stderr, " __TBF_error_found.\n");
  exit(1);
}
\end{minted}

When \mintinline{c}{__VERIFIER_error} is called during a test~execution,
\tbf reports that the test~suite successfully covered the error.
This way, \tbf enables us to benchmark
the bug-finding capabilities of test~generators for~C with the sv-benchmarks.

\paragraph{Results}

We compare the results of the test~generators executed with \tbf
to the formal~verifiers of \svcomp.
We use the same machines and resource~limits as \svcomp~2017:
\SI{900}{\second} of CPU~time and \GB{15} of memory.

To make sure that formal~verifiers do not get an advantage
because they are imprecise and guess lucky,
we make sure that they are of high precision (only 3~false~alarm
across all \num{4203} error-free verification~tasks)
and we run witness~validation to confirm the reported bugs.
The results show that the union of all formal~verifiers is able to find bugs in
more benchmark~tasks than the union of all test~generators:
All formal~verifiers find bugs in \num{979}~tasks (with confirmed results),
while test-generators only find bugs in \num{887}~tasks.
The best individual formal~verifier
(based on confirmed results)
is \cpaseq~\cite{CPACHECKER-SVCOMP15}:
it finds bugs in \num{857}~tasks.
The best test~generator is \klee~\cite{KLEE}:
it finds bugs in \num{826}~tasks.

The union of all tools is also better
than the union of all formal~verifiers,
with \num{1068}~tasks.
This shows us that combinations between testing
and formal~verification may be fruitful.

\paragraph{Test-Comp}
In~2018, \testcomp introduced
standardized formats for both benchmark~tasks
and test suites.
Thanks to this, we do not require \tbf anymore,
but can perform comparative~evaluations within the \testcomp framework.
In consequence, \tbf's tool~development stopped
and the repository has been archived.

\section{Tooling for Comparison of Test Generators}

\paragraph{Test-Generation Baseline}
Verifiers are often compared to the state of the art,
but no comparison to a simple-to-understand baseline is made.
This increases the risk that some easy-to-get coverage~goals or program-language features
are missed by all state-of-the-art approaches.
%\marginpar[\scriptsize\raggedleft\fullcite{PRTEST19}]{\scriptsize\raggedright\fullcite{PRTEST19}}
In the article~``\citetitle{PRTEST19}''~(\cref{apx:PRTEST19}),
we present the tool \prtest,
a plain, random test generator for C~programs.
\prtest compiles a test harness that generates uniformly distributed test inputs
against the program under test and executes it repeatedly.
Because programs are compiled with common C~compilers
and no program characteristics are considered,
\prtest supports the same language constructs that the compiler supports.
This full language support and simple approach makes \prtest
a good baseline for comparison. It participated in all iterations of \testcomp
and continuously shows that even the well-performing test-generation approaches have weaknesses.

For example, it is visible that
in \testcomp\,2022 category 
\href{https://test-comp.sosy-lab.org/2022/results/results-verified/META_Cover-Error.table.html#/table?filter=7(0*status*(category(in(correct))))}{\emph{Cover-Error}},
only the tools
\fusebmc~\cite{FUSEBMC-TESTCOMP22}, \verifuzz~\cite{VERIFUZZ-TESTCOMP22},
and \libkluzzer~\cite{LIBKLUZZER-TESTCOMP20}%
\footnote{\libkluzzer fails to solve one task that \prtest can solve due to an out-of-memory error}
can find bugs in all the tasks
that \prtest can find the bugs for.

\begin{figure}[t]
    \begin{minipage}{.43\linewidth}
        \begin{minted}[fontsize=\small]{c}
#include <stdio.h>
int main() {
    int x; scanf("%d", &x);
    int y; scanf("%d", &y);

    if (x > 0 && y > x) {
        return 0;
    } else {
        return 1;
    }
}
        \end{minted}
    \end{minipage}\hfill%
    \begin{minipage}{.55\linewidth}
        \hspace*{1cm}{\small Input: -1, 0}\vspace{-5mm}

        \begin{minted}[linenos=false,fontsize=\small]{text}
   1*:    6:   if (x > 0 && y > x) {
branch  0 taken 0% (fallthrough)
branch  1 taken 100%
branch  2 never executed
branch  3 never executed
#####:    7:        return 0;
    -:    8:    } else {
    1:    9:        return 1;
    -:   10:    }
    -:   11:}
        \end{minted}
    \end{minipage}\vspace{5mm}
    \parbox{.43\linewidth}{
        \subcaption[]{%
        Program with two conditions for single branch (in line~6)
        \label{fig:gcov-example}
        }
    }\hfill%
    \parbox{.5\linewidth}{
        \subcaption[]{%
        Branch coverage reported by \gcov: 25\,\%. The expected branch coverage is 50\,\%.%
        \label{fig:gcov-output}
        }
    }
    \caption{Example of wrong branch-coverage report by \gcov}
\end{figure}

\paragraph{Reliable Coverage Measurement}
In practice,
to measure the code~coverage of a test~suite for~C,
\gcov\footnote{\url{https://gcc.gnu.org/onlinedocs/gcc/Gcov-Intro.html}}
and \llvmcov\footnote{\url{https://www.llvm.org/docs/CommandGuide/llvm-cov.html}}
are used.
Unfortunately, these tools
do not measure branch~coverage based on the input source~code,
but based on an internal representation that splits Boolean operators into multiple branches.
\Cref{fig:gcov-example} shows an example for this:
Given the inputs \num{-1} (for~x) and \num{0} (for~y),
the program will enter the else-branch and the expected branch~coverage is \SI{50}{\%}.
But \gcov and \llvmcov both report a coverage of \SI{25}{\%}, because the condition in line~6
is split into two branches internally (\cref{fig:gcov-output}).

In addition, there are known bugs~\cite{ValidatingCoverageProfilers}
related to the tools' coverage measurement.
Last, coverage measurement is accumulated across test~executions.
But test~executions may influence each other (for example, because of file-system changes),
and tests may provoke unwanted program behavior (e.g., spawning an indefinite amount of processes,
or consuming all system~memory).

%\marginpar[\scriptsize\raggedleft\fullcite{ASE19}]{\scriptsize\raggedright\fullcite{ASE19}}
We solve this issue in article~``\citetitle{ASE19}''~(\cref{apx:ASE19}).
\testcov
is a continuation of \tbf's internal test-suite~execution.
It provides robust coverage measurement
based on the source~code (e.g., for branch~coverage)
and it supports the XML-based exchange~format for test-suites
that \testcomp established.

To execute tests reliably,
it uses components of \benchexec~\cite{Benchmarking-STTT} to isolate individual
tests from each other (both file-system changes and resource usage),
and uses program~instrumentation to explicitly label
coverage~goals in the source~code of the program~under~test.
It can then use \gcov's reliable statement-coverage~measurement
to measure the code coverage of these inserted labels.
This way, \testcov can measure branch~coverage and error~coverage
of individual test~execution and across a full test~suite.
In addition, it provides individual measurements
of run~time and memory~consumption per test.

\testcov is under active development
and has been used in all iterations of \testcomp to date,
to measure the coverage of test~suites generated by all participants.
In \testcomp~2022~\cite{TESTCOMP22},
\testcov successfully executed over \num{50832}~distinct test suites
from \num{12}~test~generators, over \num{4236}~different test-generation~tasks.


\section{From Verification~Witnesses to Tests}

We have established comparability of test~generators for C
and shown that formal~verifiers are well suited for finding bugs in software.
Next, we turn formal~verifiers into actual test~generators,
so that this potential can be used.

This has one additional advantage:
Software~developers are well versed in the use of tests and debugging~tools,
but often have little knowledge in formal verification.
So to increase the usability of formal~verifiers for bug-finding,
previous work~\cite{BLAST-test} on \blast
proposed to turn the abstract counterexamples produced by formal~verifiers
into executable tests.
%\marginpar[\scriptsize\raggedleft\fullcite{TAP18}]{\scriptsize\raggedright\fullcite{TAP18}}
Our article~``\citetitle{TAP18}''~(\cref{apx:TAP18})
follows this idea and presents a method to
convert the (abstract) violation~witnesses
that every formal~verifier of \svcomp supports into executable tests.

An executable test for a found fault
gives the highest possible confidence in the reported alarm
(because it can be directly observed through the execution),
and it makes subsequent work with the alarm easy, because
the developer can use existing tools for debugging.
%
In addition, this technique allows to use any formal~verifier
for directed test~generation (generating a single test for a violated property),
given that they support the exchange~format for witnesses%
---which all participants of \svcomp do.

The article introduces two new witness validators, \cpawtt and \fshellwtt.
Both use the article's concept:
They turn, if possible, a given violation~witness into an executable test
and execute it. If the test~execution confirms the alarm, the violation-witness is accepted.
%
Both \cpawtt and \fshellwtt participated in \svcomp since \svcomp~2018.

\section{The Current State of the Art in Bug-Finding}

With the introduced approaches and the standardization through \testcomp,
we provide a large-scale comparison of tools for
formal verification and automated test~generation
with regards to their bug-finding capabilities.
%\marginpar[\scriptsize\raggedleft\fullcite{STTT22}]{\scriptsize\raggedright\fullcite{STTT22}}
Our article~``\citetitle{STTT22}''~(\cref{apx:STTT22})
compares all \svcomp~2023 and \testcomp~2023 participants
based on the competitions' open data~\cite{SVCOMP23-RESULTS-artifact,TESTCOMP23-RESULTS-artifact}.
This gives highest confidence on the reported results:
tool developers can configure the tools for the competition
and both the tools and the data is peer~reviewed.

Compared to our previous study~\cite{HVC17},
we do not bother to report bug reports from formal~verification that are not confirmed
by at least one validator in \svcomp.
In addition, we report how many of the bug reports from formal~verification can be
confirmed through execution-based validation.
Last, we check whether the test~generators that participate in \testcomp
are tuned towards the coverage goal \benchProperty{coverage-error-call},
and how good the test~suites generated for \benchProperty{coverage-branches}
are in finding bugs.

Our study shows the following:
(1)~Many automated test~generators
have adapted hybrid approaches to bug~finding, using multiple formal techniques
or mixing formal and dynamic analysis techniques.
The winner of \testcomp~2023, \fusebmc~\cite{FUSEBMC-TESTCOMP22},
combines input~fuzzing~\cite{FuzzingSurvey} with bounded model checking~\cite{BMC}.
(2)~Automated test~generators have greatly improved in the past years
and surpass \svcomp participants; but formal verification is still competitive.
(3)~Execution-based validation of verification results works well for a high number of results.
This gives bug reports of formal~verification the same level of confidence as
bugs revealed through test execution.
(4)~ Regarding targeting error calls, our results are not surprising, but give confidence:
Almost all \testcomp participants target the looked-for error~call in their test~generation.
