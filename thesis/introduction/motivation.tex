%\epigraph{\textit{``Have you tried turning it off and on again?''}}{%Roy Trenneman, The IT Crowd
%}
%
Our lifes have been infiltrated by software.
Odds are there is a smartphone in your arm's reach
or a small computer around your wrist;
planes, cars and
%(electric)
bicycles increasingly rely on software;
%that some call artificial~intelligence %based on statistics
the statistical methods
of machine learning
are used for personal~entertainment,
healthcare, research, and jurisdiction.
Software supercharges humanity.
%enables humanity
%to do things unthinkable a few years back.

This reliance
can have grave consequences
when software malfunctions:
% 2014
(a)~The heartbleed~bug~\cite{Heartbleed} in the cryptography library OpenSSL
% 2021
and the log4j~bug\footnote{\url{https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832}}
in the logging~library~log4j 
introduced severe security holes in everyday web services.
The heartbleed~bug was fixed two years after its introduction,
and the log4j~bug was mitigated only eight~years after its introduction.
%
(b)~The Mariner~1 spacecraft
(due to a faulty equation)~\cite{MarinerVenus1962},
the Mars Climate Orbiter
(due to wrong unit conversion)~\cite{McoReport},
and the Ariane~5 rocket
(due to an integer~overflow)~\cite{Ariane5Report}
all three malfunctioned due to software bugs,
leading to a combined~loss of about US\$\,600~million.
% 2016
(c)~Statistical bugs
in the brain-imaging software~AFNI~\cite{AfniFailure}
question the results of about every tenth publication on brain-imaging research
within a 15~year period,
and a bug in radiation-therapy machine Therac-25~\cite{Therac25}
killed three~patients during treatment with lethal radiation overdoses.
The ``fixed'' software introduced a new integer overflow
that killed once more.

These examples are extremes,
but, with lesser consequences,
software does not behave as intended on a daily basis:
software bugs are so common that we discard them as a law of nature: \emph{software has bugs}.
But it should not have to be this way.

To be able to fully trust software, many things have to come together.
One piece of this bigger puzzle is the verification of \emph{functional safety} of software:
the check that software behaves as intended by their creators.
%
Software systems are increasingly complex, and manual verification does not scale well.
In turn, we need tools that automatically and reliably verify large-scale software for us.
%
The holy~grail we are working towards is a tool that fully automatically verifies
any given software system for us at the push of a button, and tells us whether it is safe or not.

Because of its wide use in industry%
% and embedded systems
,
we only consider tools for verification of C~programs.
We look at two categories of automatic software verification:
Automated test generation (for software testing),
and formal verification (which creates formal correctness proofs or counterexamples).

Approaches to automatically verify software~\cite{HBMC-book,SVCOMP22,TESTCOMP22}
have been extensively researched since Alan~Turing~\cite{Turing49},
but each existing verification~technique has different strengths and weaknesses.

First, we need to understand these strengths and weaknesses.
This requires the experimental evaluation of techniques on a level playing field.
%
Tools for test~generation in C have been living next to each other
in isolation and had no standardized formats for testing or information exchange.
But this is a fundamental necessity for fair evaluation (and later cooperation).
We first work towards standardizing formats and improving the comparability
of the different tools through reliable experimental evaluation.

Next, cooperation is necessary:
two complementing techniques should not just
work side by side, but cooperate, exchanging information to help each other solve tasks
that neither of the techniques could solve on its own.
%
Many research~\cite{CoVeriTest-STTT,MayMustAnalysis,DART,HybridConcolicTesting,AbstractionConcolicTesting,CPACHECKER-SVCOMP14,INFER,CollaborativeVerification,HybridAugmentationFramework,ProgramPartitioning}
has combined verification techniques,
but these approaches implement combinations as cohesive units,
making them monolithic; inflexible and costly to exchange individual techniques.

We contrast this:
We believe that cooperation must be possible through clearly defined interfaces
and in a plug-and-play fashion. This avoids technology lock-in
and enables quick adaption of new technologies.
Such an off-the-shelf capability for cooperation requires formats for information exchange.
%
So, as a second step, we inspect an existing, but hardly used exchange format
for formal verification: condition automata~\cite{ConditionalModelChecking}.
We make it widely applicable, explore the opportunities it yields,
and transfer the concept from formal~verification to test~generation.

Last, we show how existing, cohesive verification techniques can be decomposed into
individual components with clearly defined interfaces, increasing their flexibility.

We hope that these building blocks contribute towards safer software.