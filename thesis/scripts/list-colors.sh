#!/bin/zsh

for i in **/*.tex; do
    ag -no "fill=[^,}\]]*" "$i" | cut -c 6-
    ag -no "draw=[^,}\]]*" "$i" | cut -c 6-
    ag -no "color=[^,}\]]*" "$i" | cut -c 7-
done | sort -u