#!/bin/bash

## Requires pdftk and curl

set -Eeuo pipefail
IFS=$'\n'

SCRIPTNAME=$(basename "$0")
SUCC_COLOR='\033[0;32m'
FAIL_COLOR='\033[0;31m'
NO_COLOR='\033[0m'

if [[ -z "${1:-}" ]]; then
    1>&2 echo -e "Usage:\n\t$SCRIPTNAME PDF_FILE"
    exit 22
fi

PDF="$1"
UNCOMPRESSED=$(mktemp)
pdftk "$PDF" cat output "$UNCOMPRESSED" uncompress
# Remove uncompressed pdf file on script exit
trap 'rm $UNCOMPRESSED' EXIT

EXITCODE=0
for LINK in $(strings "$UNCOMPRESSED" | grep URI | grep http | sed -E -e 's/^.*\((https?:\/\/.*)\).*$/\1/' -e 's/\\\(/(/g' -e 's/\\\)/)/g' | sort -u); do
    if curl --output /dev/null --silent --head --fail "$LINK"; then
        # "Good" sounds a bit strange as a status report, but it has the same number of characters as FAIL,
        # so it makes the output align well.
        echo -e "${SUCC_COLOR}GOOD: $LINK${NO_COLOR}"
    else
        echo -e "${FAIL_COLOR}FAIL: $LINK${NO_COLOR}"
        EXITCODE=1
    fi
done
exit $EXITCODE


