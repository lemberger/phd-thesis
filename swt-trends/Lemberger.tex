\documentclass[twocolumn]{article}
\input{trendsstyle.tex}
\pagestyle{empty}
\newcommand{\stt}{Soft\-ware\-tech\-nik-Trends}
\newcommand{\BI}{\begin{itemize}}
\newcommand{\EI}{\end{itemize}}
\raggedbottom

\usepackage[backend=biber,maxbibnames=99,sortcites=true,bibencoding=utf8,doi=true,isbn=false,url=false,giveninits=true,sortcites=false,style=numeric]{biblatex}
\AtBeginBibliography{\footnotesize}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{tikz}
\usepackage{xcolor}
\usepackage{cleveref}

\usetikzlibrary{automata,positioning,shapes.geometric}

\bibliography{Lemberger_references.bib}

\definecolor{colorBorders}{HTML}{333333}
\definecolor{colorUnknown}{HTML}{FFF38A}
\colorlet{colorCondNode}{colorUnknown}

\tikzset{
  rounded rectangle/.style={rounded corners=2pt},
  object/.style={rounded corners=1pt, align=center, fill=white},
  artifact/.style={object, draw, document, minimum height=9mm, minimum width=7mm, inner sep=2pt},
  tool/.style={object, draw, rounded rectangle, minimum width=1.5cm, minimum height=1cm},
  small tool/.style={tool, minimum height=1cm, minimum width=1cm, inner sep=5pt, rounded rectangle},
  database/.style={
      cylinder,
      shape border rotate=90,
      aspect=0.2,
      draw,
      inner sep=4pt,
      
    },
    edge/.style={
      draw, ->, thick
    },
}
% Document symbol
% Copied from the TikZ-Manual
  \pgfdeclareshape{document}{
  \inheritsavedanchors[from=rectangle]
  % this is nearly a rectangle
  \inheritanchorborder[from=rectangle]
  \inheritanchor[from=rectangle]{center}
  \inheritanchor[from=rectangle]{north}
  \inheritanchor[from=rectangle]{south}
  \inheritanchor[from=rectangle]{west}
  \inheritanchor[from=rectangle]{east}
  \inheritanchor[from=rectangle]{north west}
  \inheritanchor[from=rectangle]{north east}
  \inheritanchor[from=rectangle]{south west}
  \inheritanchor[from=rectangle]{south east}
  \backgroundpath{
  % this is new
  % store lower right in xa/ya and upper right in xb/yb
  \southwest \pgf@xa=\pgf@x \pgf@ya=\pgf@y
  \northeast \pgf@xb=\pgf@x \pgf@yb=\pgf@y
  % compute corner of ``flipped page''
  \pgf@xc=\pgf@xb \advance\pgf@xc by-8pt
  % this should be a parameter
  \pgf@yc=\pgf@yb \advance\pgf@yc by-8pt
  % construct main path
  \pgfpathmoveto{\pgfpoint{\pgf@xa}{\pgf@ya}}
  \pgfpathlineto{\pgfpoint{\pgf@xa}{\pgf@yb}}
  \pgfpathlineto{\pgfpoint{\pgf@xc}{\pgf@yb}}
  \pgfpathlineto{\pgfpoint{\pgf@xb}{\pgf@yc}}
  \pgfpathlineto{\pgfpoint{\pgf@xb}{\pgf@ya}}
  \pgfpathclose
  % add little corner
  \pgfpathmoveto{\pgfpoint{\pgf@xc}{\pgf@yb}}
  \pgfpathlineto{\pgfpoint{\pgf@xc}{\pgf@yc}}
  \pgfpathlineto{\pgfpoint{\pgf@xb}{\pgf@yc}}
  \pgfpathlineto{\pgfpoint{\pgf@xc}{\pgf@yc}}
  }
}

\newcommand{\testcomp}{Test-Comp}
\newcommand{\testcov}{TestCov}
\newcommand{\esbmc}{ESBMC}

\begin{document}

\twocolumn[{
\Large
\centerline{Cooperative Approaches Across Test Generation and Formal Software Verification}
~\\[-2mm]
\normalsize
\centerline{Thomas Lemberger, LMU M\"unchen, Geschwister-Scholl-Platz~1, 80539, M\"unchen}
~\\
}]

\begin{abstract}
In the last decade, powerful techniques were developed that
either automatically generate tests for software, or
automatically verify software with formal methods.
%But because all developed techniques have some major weaknesses,
In both areas it is common to combine different techniques
to leverage their strengths and mitigate their weaknesses.
This happens through costly, proprietary reimplementations within a single tool.
%that may miss existing important optimizations from the model implementation.
This thesis~\cite{LembergerCoop} contrasts this and provide concepts that enable
an inexpensive and fast off-the-shelf cooperation of standalone tools through standardized exchange formats.
\end{abstract}
%\keyword{Software verification}

\section{Introduction}
There are two major methods for software verification: testing and formal verification.
To increase our confidence in software on a large scale,
we require tools that apply these methods automatically and reliably.
Testing is a widespread engineering technique,
and many tools for automatic test generation exist.
But testing can never provide full confidence in software---%
it can show the presence of bugs, but not their absence.
In contrast, formal verification can.
Unfortunately, even successful formal-verification~techniques
either scale poorly or
rely on heuristics that fail if the used heuristic does not suit
the verification task.
%
As workaround, combinations of multiple techniques try
to combine their strengths and mitigate their weaknesses.
But these combinations are often designed as cohesive, monolithic units.
This makes them inflexible and it is costly to replace components.

We propose an alternative approach:
If tools support the same input and output formats,
we can use these formats and combine existing tools without
any adjustments (off-the-shelf).

For formal verification,
the International Competition on Software Verification (SV-COMP)~\cite{SVCOMP24}
has established conventions for both input tasks
and result formats. These conventions are supported by more than 40~tools to date.
But for test generation,
no such conventions exist in the C~ecosystem%
\footnote{We focus on verifiers and test generators for the C programming language}.

In the scope of our work, we first work towards
input and output conventions for test generation.
We then go beyond the conventions of tool competitions
and propose a technique to encode partial verification
results directly into the program code.
Based on this, we introduce new concepts for
the off-the-shelf cooperation between test generators and formal verifiers
and show their flexibility through different examples.

%Most notably
%we turn any formal verifier into a test generator,
%and show how cooperation can be used to decompose existing verification algorithms
%into tool-based cooperative techniques.

\section{Standardizing Test Generation}

SV-COMP~\cite{SVCOMP24} is an off-site tool competition that
compares formal verifiers on a predefined benchmark set and in a controlled environment.
SV-COMP defines how non-deterministic values are introduced into programs,
how program errors are signaled, and in which formats
formal verifiers have to report alarms and proofs.
These rules are partially based on sv-benchmarks,
the largest available benchmark set for verification of C~programs.

To establish common standards in test generation,
we transferred our experience with SV-COMP.
An initial comparative study~\cite{TestingModelChecking}
worked towards the establishment
of the First International Competition on Software Testing (Test-Comp)~\cite{TESTCOMP19}.
Test-Comp uses the sv-benchmarks
as input programs and defines
a new, common exchange format for test suites.
All participants of Test-Comp must understand input programs
according to the sv-benchmarks conventions,
and produce test suites in the expected exchange format.
These standards open all competition participants
to cooperation on the tool level.

%In an initial work,
%we created the framework TBF~\cite{TestingModelChecking}
%to adapt a selection of existing test generators to the standards
%of SV-COMP.
%\textsc{AFL}~\cite{AFLfast},
%\textsc{CPATiger}~\cite{REUSE-Reach},
%\textsc{Crest-PPC}~\cite{CRESTPPC},
%\textsc{FShell}~\cite{FShell},
%and \textsc{Klee}~\cite{KLEE}.
We contribute two tools to Test-Comp:
the plain random tester \textsc{PRTest}~\cite{PRTEST-TESTCOMP19}
and the test-suite executor \textsc{TestCov}~\cite{TestCov}.
\textsc{PRTest} samples test inputs
from a uniform random distribution.
This purely random approach serves as a baseline for tool comparisons.
\textsc{TestCov} provides robust test-suite execution and coverage measurement
for different coverage criteria. It is used for the coverage-measurement
and score computation in Test-Comp.

\section{Concepts for Cooperative Software Verification}

\begin{figure}[t]
    \parbox{.48\linewidth}{%
    \scalebox{1}{
\begin{tikzpicture}[node distance=5mm]
    \node[artifact] (test-goal) {$\Psi_0$};

    %\node[artifact, below = 5mm of test-goal] (spec) {$\varphi$};

    %\node[artifact, below = 5mm of spec] (P) {P};

    \node[tool, right = of test-goal] (T1) {Verifier 1};
    %\node[tool, below = of T1] (Ti) {$\text{Cond. Tester}_i$};
    \node[tool, below = of T1] (Tn) {Verifier 2};

    %\draw[dashed, rounded corners=5pt] ($(T1.west) + (-0.45, 1.25)$) rectangle ($(Tn.east) + (0.45, -1.25)$);

    \node[artifact, right = of T1] (test1) {$\Psi_1$};
    %\node[below = 0mm of test1] (test1-label) {Test Cases};

    %\node[artifact, right = of Ti] (testi) {$\Psi_2$};
    %\node[below = 0mm of testi] (testi-label) {Test Cases};

    \node[artifact, right = of Tn] (testn) {$\Psi_n$};

    \draw[edge] (test-goal) -- (T1);
    %\draw[edge] (P.east) -- ++(0.3, 0) |- (T1.180);
    %\draw[edge] (spec.east) -- ++(0.7, 0) |- (T1.200);
   % \draw[edge] (P.east) -- ++(0.3, 0) |- (Ti.170);
   % \draw[edge] (spec.east) -- ++(0.7, 0) |- (Ti.190);
    %\draw[edge] (P.east) -- ++(0.3, 0) |- (Tn.170);
    %\draw[edge] (spec.east) -- ++(0.7, 0) |- (Tn.190);
    \draw[edge] (T1) -- (test1);
    \draw[edge] (test1) -- (Tn);
    %\draw[edge] (Ti) -- (testi);
    %\draw[edge] (testi) -- (Tn);
    \draw[edge] (Tn) -- (testn);

    %\node[database, below = 5mm of Tn] (test-db) {Test Suite};

    %\draw[edge, dashed] (T1) -- (Tn.north) (Tn.south) -- (test-db.north);
    %\draw[edge, dashed] (Ti.270) -- (test-db.north -| Ti.270);
    %\draw[edge, dashed] (Tn.310) -- (test-db.north -| Tn.310);

    \end{tikzpicture}
    }
    }\hfill%
    \parbox{.39\linewidth}{%
        \scalebox{1}{
    \begin{tikzpicture}[node distance=5mm]
    \node[artifact] (test-goal) {$\Psi$};

    \node[tool, right = of test-goal] (T1) {Verifier};
    %\draw[dashed, rounded corners=5pt] ($(T1.west) + (-0.45, 1.25)$) rectangle ($(Tn.east) + (0.45, -1.25)$);


    %\node[database, below = 8mm of T1] (test-db) {Test Suite};

    %\path[edge] (P.east) -- (T1.200);
    \path[] (test-goal.east |- T1.170) edge[edge, bend left=50] (T1.170);
    %\path[edge] (test-spec.east) -- (T1);
    \path[] (T1.190) edge[edge, bend left=50] (test-goal.east |- T1.190);
    %\path[edge, dashed] (T1.245) -- (test-db.north -| T1.245);
    %\path[edge, dashed] (T1.270) -- (test-db.north -| T1.270);
    %\path[edge, dashed] (T1.295) -- (test-db.north -| T1.295);

    \end{tikzpicture}
        }
    }\\
    \caption{Examples of cooperative verification: Sequential and cyclic combination}
    \label{fig:example}
\end{figure}

The wide tool support of the SV-COMP and Test-Comp standards
enable the off-the-shelf combination of verification tools.
But formal verifiers do not have to produce partial results.
Because of this, possible combinations are limited to techniques that
only consider verification runs that successfully finish (with an alarm or an proof).
If a formal verifier's run does not finish successfully,
the performed work is simply discarded.

Conditional Model Checking~\cite{ConditionalModelChecking} is a technique
that makes verifiers exchange partial verification results through \emph{conditions}.
This could lead to more cooperation possibilities and information reuse,
but there is little tool support.
To mitigate this limitation,
we propose a reducer-based construction of conditional model checkers~\cite{ReducerCMC}:
partial verification results are encoded directly into the program code,
so that any existing formal verifier can consume them.
We apply the same concept to test generators through the concept
of \emph{conditional testing}~\cite{ConditionalTesting}.

This construction enables a strong information exchange
between verifiers.
\Cref{fig:example} shows two example compositions: A sequential
composition of verifiers, and a cyclic composition of a verifier.
Verifiers start with some initial knowledge~$\Psi_0$ about the verification task
(by default: nothing),
compute new knowledge~$\Psi_1$, and, if the verification task is not solved yet,
communicate their computed knowledge to the next verifier.
Note that any such composition is a verifier itself,
and compositions can be arbitrarily nested.

To make formal verifiers also usable for test generation,
we introduce the concept of \emph{Witness2Test}~\cite{ExecutionBasedValidation}
that transforms alarms reported in the SV-COMP conventions into executable tests.

The proposed concepts can not only improve the effectiveness of verification~\cite{ReducerCMC,ConditionalTesting},
but can also be used for incremental verification~\cite{DiffCond}.
On the example of counterexample-guided abstraction refinement~\cite{ClarkeCEGAR},
we show~\cite{CCEGAR} how existing, strongly coupled techniques in software verification can
be decomposed into stand-alone components that cooperate
through standardized exchange formats.

\section{Results}
All our work is backed by rigorous implementation of the proposed concepts
and thorough experimental~evaluations that demonstrate the benefits of our work.
We were able to show that cooperative verification improves the effectiveness
of verification beyond the prior state of the art~\cite{ReducerCMC,ConditionalTesting}.
\testcov{} successfully drives the coverage measurements of \testcomp{} since 2019.
And the test generator~FuSeBMC~\cite{FUSEBMC-TESTCOMP22} builds
on our concept of conditional testing to achieve a cooperation between
the bounded model checker \esbmc~\cite{ESBMC-kind}
and two fuzz testers~\cite{MAP2CHECK}.
This combination won \testcomp~2022, 2023, and 2024.

The introduced standards and tools also
improve the comparability of automated verifiers,
enable cooperation between a large array of existing verification tools,
and create new opportunities for research in cooperative verification.

\printbibliography

\end{document}
